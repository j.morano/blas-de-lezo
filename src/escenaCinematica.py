# -*- encoding: utf-8 -*-

import pygame
from escena import *
from gestorRecursos import *

from GUI import PantallaCinematica, Parrafo, Boton, BotonAtras, BotonSiguiente


################################################################################


class BotonJugar(Boton):
    ''' Boton para empezar a jugar '''

    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, 'JUGAR', GestorRecursos.font, 40, GestorRecursos.WHITE, (500, 550))


    def accion(self):
        self.pantalla.escenaCinematica.avanzarCinematica() 



class PantallaCinematicaPrimera(PantallaCinematica):
    ''' Primera pantalla de la cinematica inicial '''

    def __init__(self, escenaCinematica):

        # Se carga el fondo
        PantallaCinematica.__init__(self, escenaCinematica, "cinematica1final.jpg")

        # Se crea y añade el boton siguiente a la lista de elementosGUI
        botonSiguiente = BotonSiguiente(self)
        self.elementosGUI.append(botonSiguiente)

        # Se crea el texto de la cinematica y se añade como parrafo
        texto_cinematica = ["Durante el conflicto de la Guerra del", \
                            "Asiento, la flota Imperial española lucha", \
                            "encarnizadamente contra los invasores ", \
                            "ingleses para mantener el control", \
                            "sobre las tierras del Caribe..."]
        Parrafo.escribirParrafo(self, texto_cinematica)



class PantallaCinematicaSegunda(PantallaCinematica):
    ''' Segunda pantalla de la cinematica inicial '''

    def __init__(self, escenaCinematica):

        # Se carga el fondo
        PantallaCinematica.__init__(self, escenaCinematica, "cinematica2final.jpg")

        # Se crean y añaden los botones atras y siguiente
        botonAtras = BotonAtras(self)
        botonSiguiente = BotonSiguiente(self) 
        self.elementosGUI.append(botonSiguiente)
        self.elementosGUI.append(botonAtras)

        # Se crea el texto de la cinematica y se añade como parrafo
        texto_cinematica = ["En este conflicto, durante el sitiado", \
                            "de Cartagena de Indias, Blas de Lezo", \
                            "conseguirá una victoria muy importante", \
                            "para el Imperio a pesar de su", \
                            "gran inferioridad numérica..."]
        Parrafo.escribirParrafo(self, texto_cinematica)



class PantallaCinematicaTercero(PantallaCinematica):
    ''' Tercera pantalla de la cinematica inicial '''

    def __init__(self, escenaCinematica):

        # Se carga el fondo
        PantallaCinematica.__init__(self, escenaCinematica, "cinematica3final.jpg")

        # Se crean y añaden los botones atras y siguiente
        botonAtras = BotonAtras(self)
        botonSiguiente = BotonSiguiente(self) 
        self.elementosGUI.append(botonSiguiente)
        self.elementosGUI.append(botonAtras)

        # Se crea el texto de la cinematica y se añade como parrafo
        texto_cinematica = ["Tras la victoria y durante la retirada", \
                            "de los ingleses, nadie se esperaba que la", \
                            "retaguardia británica fuera capaz de", \
                            "capturar a Blas de Lezo y llevarlo preso", \
                            "al Castillo de Norwich en Inglaterra..."]
        Parrafo.escribirParrafo(self, texto_cinematica)


class PantallaCinematicaCuarto(PantallaCinematica):
    ''' Cuarta pantalla de la cinematica inicial '''

    def __init__(self, escenaCinematica):

        # Se carga el fondo
        PantallaCinematica.__init__(self, escenaCinematica, "cinematica4final.jpg")

        # Se crean y añaden los botones atras y jugar
        botonAtras = BotonAtras(self)
        botonJugar = BotonJugar(self) 
        self.elementosGUI.append(botonJugar)
        self.elementosGUI.append(botonAtras)

        # Se crea el texto de la cinematica y se añade como parrafo
        texto_cinematica = ["Dentro del castillo, Blas de Lezo tendrá", \
                            "que ingeniárselas para trazar un plan de", \
                            "huida y vencer al alcaide de la prisión,", \
                            "para así poder volver al Virreinato a", \
                            "celebrar la victoria."]
        Parrafo.escribirParrafo(self, texto_cinematica)



class EscenaCinematica(EscenaPygame):
    ''' Escena pygame de la cinemática inicial '''

    def __init__(self, director):

        EscenaPygame.__init__(self, director)

        # Para efectos en la primera pantalla de la cinemática
        self.primero = True

        # Para saber si se ha transicionado hacia la siguiente escena
        self.transicion = True

        # Creamos las pantallas y las introducimos en la lista. Inicializamos un
        # indicador de la pantalla actual y mostramos esa pantalla
        self.listaPantallas = []
        self.listaPantallas.append(PantallaCinematicaPrimera(self))
        self.listaPantallas.append(PantallaCinematicaSegunda(self))
        self.listaPantallas.append(PantallaCinematicaTercero(self))
        self.listaPantallas.append(PantallaCinematicaCuarto(self))
        self.pantallaActual = 0
        self.mostrarPantallaInicial()


    def update(self, *args):
        ''' Como cada pantalla es estática, no se actualiza nada '''
        return


    def eventos(self, lista_eventos):
        ''' Metodo para tratar los eventos de esta escena '''

        # Eventos para salir del programa
        for evento in lista_eventos:
            if evento.type == KEYDOWN:
                if evento.key == K_ESCAPE:
                    self.salirPrograma()
            elif evento.type == pygame.QUIT:
                self.director.salirPrograma()

        # Se delega el resto de eventos a las pantallas concretas
        self.listaPantallas[self.pantallaActual].eventos(lista_eventos)


    def dibujar(self, pantalla):
        ''' Metodo para dibujar las pantallas '''

        # Si es la primera vez que se dibuja se hacen los efectos que se indican
        if (self.pantallaActual) == 0 and self.primero:
            self.primero = False
            GestorRecursos.BajarMusica(1000)
            pantalla.fill(GestorRecursos.BLACK)
            pygame.display.update()
            GestorRecursos.CargarMusica('Pirate_posible_Intro_Cinematica_Mapa.mp3', 0.35, -1)

        # El resto del dibujado se delega a la pantalla actual
        self.listaPantallas[self.pantallaActual].dibujar(pantalla)


    def avanzarCinematica(self):
        ''' Metodo para avanzar a la siguiente cinematica '''

        # Si es la última pantalla de la cinemática se inicia la siguiente
        # escena
        if self.pantallaActual >= (len(self.listaPantallas)-1):

            # Si se ha transicionado anteriormente ya no se hace nada
            if self.transicion:
                print('¡A jugar!')
                self.director.salirEscena() #Desapila y empiezan los mapas
                self.transicion = False

        # Sino es la última pantalla, se pasa a la siguiente pantalla de la
        # cinemática
        else:
            self.pantallaActual+=1


    def retrasarCinematica(self):
        ''' Metodo para ir hacia la pantalla anterior en la cinemática '''

        self.pantallaActual-=1


    def mostrarPantallaInicial(self):
        ''' Metodo para situar la pantalla inicial en la primera '''

        self.pantallaActual = 0