# -*- coding: utf-8 -*-
#!/usr/bin/python3

import pygame
from pygame import *
from gestorRecursos import *
from escena import *
from escenaCinematicaFinal import EscenaCinematicaFinal
from escenaCinematica import EscenaCinematica, Parrafo
from fase4 import Fase4
from fase import Fase
from GUI import ElementoGUI, TextoGUI, Boton

import json

################################################################################


#Botones con las acciones definidas del menu principal

#Permita inicializar el juego y comenzar la cinemática.
class BotonJugar(Boton):
    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, 'EMPEZAR', GestorRecursos.font, 60, GestorRecursos.WHITE, (240, 320))
    def accion(self):
        self.pantalla.menu.ejecutarJuego()

#Permite cambiar la pantalla del menú a la de tutorial, con la información en texto en otra pantalla.
class BotonTutorial(Boton):
    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, 'TUTORIAL', GestorRecursos.font, 60, GestorRecursos.WHITE, (230, 460))
    def accion(self):
        self.pantalla.menu.iniciarTutorial()

#Permite finalizar la ejecución del juego.
class BotonSalir(Boton):
    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, 'SALIR', GestorRecursos.font, 60, GestorRecursos.WHITE, (310, 520))
    def accion(self):
        self.pantalla.menu.salirPrograma()

#Permite volver a la pantalla principal del menú (con todas las opciones) desde la pantalla de tutorial del menú.
class BotonAtras(Boton):
    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, 'ATRÁS', GestorRecursos.font, 60, GestorRecursos.WHITE, (290, 590))
    def accion(self):
        self.pantalla.menu.mostrarPantallaInicial()

#Permite recuperar el estado de una partida guardada.
class BotonSeguir(Boton):
    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, 'SEGUIR JUEGO', GestorRecursos.font, 60, GestorRecursos.WHITE, (160, 390))
    def accion(self):
        self.pantalla.menu.seguirJuego()
 
 #-------------------------------------------------


#Texto de inicio en la pantalla del menu.

class TextoTitulo(TextoGUI):
    def __init__(self, pantalla, color, posicion):
        fuente = GestorRecursos.font
        TextoGUI.__init__(self, pantalla, fuente, color, "Lezo: una victoria, una venganza", posicion)
    def accion(self):
        return

# -------------------------------------------------
# Clase PantallaGUI y las distintas pantallas

class PantallaGUI:
    def __init__(self, menu, nombreImagen):
        self.menu = menu
        # Se carga la imagen de fondo
        (self.imagen, self.imagen_rect)= GestorRecursos.CargarImagen(nombreImagen, True)
        # Se tiene una lista de elementos GUI
        self.elementosGUI = []
        # Se tiene una lista de animaciones
        self.animaciones = []

    def eventos(self, lista_eventos):
        for evento in lista_eventos:
            if evento.type == MOUSEMOTION:
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        elemento.actualizarColor(self.menu.screen, GestorRecursos.ORANGE)
                    else:
                        elemento.actualizarColor(self.menu.screen, GestorRecursos.WHITE)
            if evento.type == MOUSEBUTTONDOWN:
                self.elementoClic = None
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        self.elementoClic = elemento
            if evento.type == MOUSEBUTTONUP:
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        if (elemento == self.elementoClic):
                            elemento.accion()

    def dibujar(self, pantalla):
        # Dibujamos primero la imagen de fondo
        pantalla.blit(self.imagen, self.imagen_rect)
        # Después las animaciones
        for animacion in self.animaciones:
            animacion.dibujar(pantalla)
        # Después los botones/textos
        for elemento in self.elementosGUI:
            elemento.dibujar(pantalla)

#Pantalla principal del menu con todos los componentes.
class PantallaInicialGUI(PantallaGUI):
    def __init__(self, menu):
        PantallaGUI.__init__(self, menu, 'FondoMenuPrincipal.jpg')
        GestorRecursos.CargarMusica('PirateIntroMenu.mp3', 0.5, -1)
        # Creamos los botones y los metemos en la lista
        botonJugar = BotonJugar(self)
        botonTutorial = BotonTutorial(self)
        botonSalir = BotonSalir(self)
        botonSeguir = BotonSeguir(self)
        textoTituloSombra = TextoTitulo(self, GestorRecursos.BLACK, (42,132))
        textoTituloColor = TextoTitulo(self, GestorRecursos.ORANGE, (40,130))
        self.elementosGUI.append(botonSeguir)
        self.elementosGUI.append(botonJugar)
        self.elementosGUI.append(botonTutorial)
        self.elementosGUI.append(botonSalir)
        self.elementosGUI.append(textoTituloSombra)
        self.elementosGUI.append(textoTituloColor)

#Otro elemento adicional de las pantallas que permite incorporar iconos/imagemes pequeñas a la pantalla
class Imagen(ElementoGUI):
    def __init__(self, imagen, rect, x, y):
        self.x = x
        self.y = y
        self.imagen = imagen
        self.rect = rect

    def dibujar(self, pantalla):
        pantalla.blit(self.imagen, (self.x, self.y))

    def actualizarColor(self, pantalla, color_nuevo):
        pass

    def accion(self):
        pass


#Pantalla secundaria del menú para mostrar el tutorial del juego.
class PantallaTutorialGUI(PantallaGUI):
    def __init__(self, menu):
        PantallaGUI.__init__(self, menu, 'FondoMenuPrincipalTutorial.jpg')
        botonAtras = BotonAtras(self)
        self.elementosGUI.append(botonAtras)

        texto_tutorial = ["- Utiliza las flechas de dirección para moverte.", \
                          "- Utiliza espacio para atacar a los enemigos.", \
                          "- Ten cuidado con las plataformas punzantes y el fuego.", \
                          "                 ¡Te causarán daño!", \
                          "", \
                          "- Busca las llaves cuando sea necesario.", \
                          "", \
                          "- Recoge los objetos, con ellos ganarás puntos extra.", \
                          "", \
                          "- Encuentra a Carlos Desnaux, ¡quizás te ayude!",
                          "- Aprovecha tus ataques, la barra azul te dirá cuándo",
                          "  puedes hacerlos.",
                          "- Y recuerda no pasarte de tiempo..."]
        
        Parrafo.escribirParrafo(self, texto_tutorial, 25, 35, (30,30))

        (llave,llave_rect) = GestorRecursos.CargarImagen("llave.png",True,-1, escalado=(84, 60))
        self.elementosGUI.append(Imagen(llave, llave_rect, 520, 200))

# -------------------------------------------------
# Clase Menu, la escena en sí, en Pygame

class Menu(EscenaPygame):

    def __init__(self, director):
        # Llamamos al constructor de la clase padre
        EscenaPygame.__init__(self, director);
        # Creamos la lista de pantallas
        self.listaPantallas = []
        # Creamos las pantallas que vamos a tener
        #   y las metemos en la lista
        self.listaPantallas.append(PantallaInicialGUI(self))
        self.listaPantallas.append(PantallaTutorialGUI(self))
        # En que pantalla estamos actualmente
        self.mostrarPantallaInicial()

    def update(self, *args):
        return

    def eventos(self, lista_eventos):
        # Se mira si se quiere salir de esta escena
        for evento in lista_eventos:
            # Si se quiere salir, se le indica al director
            if evento.type == KEYDOWN:
                if evento.key == K_ESCAPE:
                    self.salirPrograma()
            elif evento.type == pygame.QUIT:
                self.director.salirPrograma()

        # Se pasa la lista de eventos a la pantalla actual
        self.listaPantallas[self.pantallaActual].eventos(lista_eventos)

    def dibujar(self, pantalla):
        self.listaPantallas[self.pantallaActual].dibujar(pantalla)

    #--------------------------------------
    # Metodos propios del menu

    #Mueve el indice de pantalla para el tutorial.
    def iniciarTutorial(self):
        print('Tutorial')
        self.pantallaActual = 1

    #Accion de salir del juego.
    def salirPrograma(self):
        print('Salir')
        self.director.salirPrograma()

    #Iniciar el juego desde el menú
    def ejecutarJuego(self):

        print('Jugar')


        # Al iniciar el juego, se escribe la partida en el inicio de la primera fase sin puntuación
        GestorRecursos.guardarEstado(0, 0)

        escenas = []

        #Escenas de fases del juego

        escenas.append(EscenaCinematica(self.director))
        escenas.append(Fase(self.director,1))
        escenas.append(Fase(self.director,2))
        escenas.append(Fase(self.director,3))
        escenas.append(Fase4(self.director, 4))
        escenas.append(EscenaCinematicaFinal(self.director))
        
        self.director.apilarEscena(self) #Apilamos el menú al final para que al acabar el juego regresemos a el.
        
        #Mientras haya escenas, las apilamos en orden inverso (pila de escenas).
        while escenas:
            self.director.apilarEscena(escenas.pop())
        

    #Metodo para continuar la partida guardada.
    def seguirJuego(self):

        print('Seguir')

        GestorRecursos.BajarMusica(1500)

        #Procesado del fichero con la información.
        json_file='PARTIDAS/partida.json'
        json_data=open(json_file)
        data = json.load(json_data)
        json_data.close()

        fase = data['fase']

        escenas = []

        GestorRecursos.guardarPuntuacion(data['puntuacion']) #Guardamos el registro de puntuación que había en el fichero en el gestor para mostrarlo en las fases.

        escenas.append(EscenaCinematica(self.director))
        escenas.append(Fase(self.director,1))
        escenas.append(Fase(self.director,2))
        escenas.append(Fase(self.director,3))
        escenas.append(Fase4(self.director, 4))
        escenas.append(EscenaCinematicaFinal(self.director))
        
        self.director.apilarEscena(self) #Apilamos el menú al final para que al acabar el juego regresemos a el.

        #En este caso, apilamos las escenas en orden inverso hasta llegar al límite apropiado según el ID de fase en la que nos encontrábamos (recuperado del fichero de partida)
        for i in range(len(escenas)-fase):
            self.director.apilarEscena(escenas.pop())


        GestorRecursos.CargarMusica('PirateMap2.mp3', 0.07, -1)



    def mostrarPantallaInicial(self):
        self.pantallaActual = 0
