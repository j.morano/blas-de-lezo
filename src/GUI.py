# -*- coding: utf-8 -*-

from gestorRecursos import *


class ElementoGUI:
    ''' Clase para elementos de interfaz '''

    def __init__(self, pantalla, rectangulo):
        self.pantalla = pantalla
        self.rect = rectangulo


    def establecerPosicion(self, posicion):
        ''' Método para establecer la posición del elemento '''
        (posicionx, posiciony) = posicion
        self.rect.left = posicionx
        self.rect.bottom = posiciony


    def posicionEnElemento(self, posicion):
        ''' Método para averiguar si la posición argumento está dentro de las
            posiciones del elemento '''
        (posicionx, posiciony) = posicion
        if (posicionx>=self.rect.left) and (posicionx<=self.rect.right) and (posiciony>=self.rect.top) and (posiciony<=self.rect.bottom):
            return True
        else:
            return False


    def dibujar(self):
        ''' Metodo abstracto '''
        raise NotImplemented("Tiene que implementar el metodo dibujar.")


    def accion(self):
        ''' Metodo abstracto '''
        raise NotImplemented("Tiene que implementar el metodo accion.")



class TextoSombraGUI(ElementoGUI):
    ''' Clase para texto con sombra '''

    def __init__(self, pantalla, fuente, tam, color, nombreTexto, posicion, offset):

        # Inicialización de atributos del texto
        self.nombreTexto = nombreTexto
        self.fuente = fuente
        self.offset = offset
        self.tam = tam
        self.color = color
        self.texto, self.sombra = GestorRecursos.text_format_shadow(nombreTexto, fuente, tam, color, GestorRecursos.GRAY)
        ElementoGUI.__init__(self, pantalla, self.texto.get_rect())
        ElementoGUI.__init__(self, pantalla, self.sombra.get_rect())
        self.establecerPosicion(posicion)


    def dibujar(self, pantalla):
        ''' Implementación del método de la superclase para dibujar el elemento
        '''

        pantalla.blit(self.sombra, self.rect.move(2,self.offset+2))
        pantalla.blit(self.texto, self.rect.move(0,self.offset))


    def actualizarColor(self, pantalla, color_nuevo):
        ''' Método para actualizar el color del texto '''
        pass


    def accion(self):
        ''' Implemenetación del método de la superclase para realizar una acción
        '''
        pass



class PantallaCinematica:
    ''' Clase para pantallas de cinemática '''

    def __init__(self, escenaCinematica, nombreImagen):

        self.escenaCinematica = escenaCinematica

        # Se carga la imagen de fondo
        (self.imagen, self.imagen_rect)= GestorRecursos.CargarImagen(nombreImagen, True)

        # Se tiene una lista de elementos GUI y de animaciones
        self.elementosGUI = []
        self.animaciones = []
        self.elementoClic = None


    def eventos(self, lista_eventos):
        ''' Método para tratar con los eventos de esta escena '''

        for evento in lista_eventos:

            # Eventos de situacion del raton
            if evento.type == MOUSEMOTION:
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        elemento.actualizarColor(self.escenaCinematica.screen, GestorRecursos.ORANGE)
                    else:
                        elemento.actualizarColor(self.escenaCinematica.screen, GestorRecursos.WHITE)

            # Eventos de clickado pero no de soltado (no confirmada la seleccion)
            if evento.type == MOUSEBUTTONDOWN:
                self.elementoClic = None
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        self.elementoClic = elemento

            # Eventos de soltado del click (confirmada la selección)
            if evento.type == MOUSEBUTTONUP:
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        if (elemento == self.elementoClic):
                            elemento.accion()


    def dibujar(self, pantalla):
        ''' Metodo para dibujar el menú de gameover'''

        # Dibujamos imagen de fondo
        pantalla.blit(self.imagen, self.imagen_rect)

        # Dibujamos las animaciones
        for animacion in self.animaciones:
            animacion.dibujar(pantalla)

        # Dibujamos los botones
        for elemento in self.elementosGUI:
            elemento.dibujar(pantalla)



class TextoLineas(TextoSombraGUI):
    ''' Clase con texto concreto del menú '''

    def __init__(self, pantalla, texto, tam, offset, pos):
        TextoSombraGUI.__init__(self, pantalla, GestorRecursos.font, tam, GestorRecursos.ORANGE, texto, pos, offset)



class Parrafo():
    ''' Clase estática para generación de párrafos de texto '''

    @staticmethod
    def escribirParrafo(pantalla, texto, tam=33, espacio=45, pos=(80,80)):
        ''' Método estático para poner texto en varias lineas, en forma de
        parrafo '''
        offset = 0
        for parte in texto:
            offset += espacio
            # print(offset)
            pantalla.elementosGUI.append(TextoLineas(pantalla, parte, tam, offset, pos))



class Boton(ElementoGUI):
    ''' Clase para botones '''

    def __init__(self, pantalla, nombreTexto, fuente, tam, color,posicion):

        # Inicialización de atributos del botón
        self.nombreTexto = nombreTexto
        self.fuente = fuente
        self.tam = tam
        self.color = color
        self.texto = GestorRecursos.text_format(nombreTexto, fuente, tam, color)
        ElementoGUI.__init__(self, pantalla, self.texto.get_rect())
        self.establecerPosicion(posicion)


    def dibujar(self, pantalla):
        ''' Ímplementación del método de la superclase para el dibujo del 
        elemento '''

        pantalla.blit(self.texto, self.rect)


    def actualizarColor(self, pantalla, color_nuevo):
        ''' Método para actualizar el color del botón '''

        self.texto = GestorRecursos.text_format(self.nombreTexto, self.fuente, self.tam, color_nuevo)
        self.dibujar(pantalla)



class BotonAtras(Boton):
    ''' Clase concreta de botón atrás '''

    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, 'ATRÁS', GestorRecursos.font, 40, GestorRecursos.WHITE, (100, 550))


    def accion(self):
        ''' Implementación del método de la superclase para realizar una acción
        '''

        self.pantalla.escenaCinematica.retrasarCinematica()
        GestorRecursos.CargarEfectoSonido('page.wav',0.15, True, 2)



class BotonSiguiente(Boton):
    ''' Clase concreta de botón siguiente '''

    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, 'SIGUIENTE', GestorRecursos.font, 40, GestorRecursos.WHITE, (500, 550))


    def accion(self):
        ''' Implementación del método de la superclase para realizar una acción
        '''

        self.pantalla.escenaCinematica.avanzarCinematica()
        GestorRecursos.CargarEfectoSonido('page.wav',0.15, True, 2)  



class TextoGUI(ElementoGUI):
    ''' Clase de texto '''

    def __init__(self, pantalla, fuente, color, texto, posicion):
        self.imagen = GestorRecursos.text_format(texto, fuente, 45, color)
        ElementoGUI.__init__(self, pantalla, self.imagen.get_rect())
        self.establecerPosicion(posicion)


    def dibujar(self, pantalla):
        ''' Ímplementación del método de la superclase para el dibujo del 
        elemento '''
        pantalla.blit(self.imagen, self.rect)

    def actualizarColor(self, pantalla, color_nuevo):
        ''' Método para actualizar el color del texto '''
        return