# -*- encoding: utf-8 -*-

import pygame
from pygame.locals import *
from escena import *
from gestorRecursos import *
import menu
# import fase1

from GUI import Boton


################################################################################

#Botones de control del menu de pausa.

#Texto de la pantalla.
class TextoPausa(Boton):
    def __init__(self, pantalla):
        # La fuente la debería cargar el estor de recursos
        Boton.__init__(self, pantalla, "PAUSA", GestorRecursos.font, 60, GestorRecursos.RED, (300, 130))

    def actualizarColor(self, pantalla, color_nuevo):
        return

    def accion(self):
        return

#Sombra del texto de la pantalla.
class TextoPausaSombra(Boton):
    def __init__(self, pantalla):
        # La fuente la debería cargar el estor de recursos
        Boton.__init__(self, pantalla, "PAUSA", GestorRecursos.font, 60, GestorRecursos.BLACK, (302, 132))

    def actualizarColor(self, pantalla, color_nuevo):
        return

    def accion(self):
        return

#Accion para reanudar la partida.
class TextoJugar(Boton):
    def __init__(self, pantalla):
        # La fuente la debería cargar el estor de recursos
        Boton.__init__(self, pantalla, "CONTINUAR", GestorRecursos.font, 40, GestorRecursos.WHITE, (270, 430))
    def accion(self):
        self.pantalla.menu.seguirJuego()


#Accion para salir de la ejecución del juego.
class TextoSalir(Boton):
    def __init__(self, pantalla):
        # La fuente la debería cargar el estor de recursos
        Boton.__init__(self, pantalla, "SALIR", GestorRecursos.font, 40, GestorRecursos.WHITE, (345, 550))
    def accion(self):
        self.pantalla.menu.salirPrograma()

#Accion para volver al menu del juego.
class TextoMenu(Boton):
    def __init__(self, pantalla):
        # La fuente la debería cargar el estor de recursos
        Boton.__init__(self, pantalla, "MENU", GestorRecursos.font, 40, GestorRecursos.WHITE, (335, 490))
    def accion(self):
        self.pantalla.menu.irMenu()



# -------------------------------------------------
# Clase PantallaPausa y las distintas pantallas

class PantallaPausa:
    def __init__(self, menu, nombreImagen):
        self.menu = menu
        # Se carga la imagen de fondo
        (self.imagen, self.imagen_rect) = GestorRecursos.CargarImagen(nombreImagen, True)
        # Se tiene una lista de elementos GUI
        self.elementosGUI = []
        # Se tiene una lista de animaciones
        self.animaciones = []

    def eventos(self, lista_eventos):
        for evento in lista_eventos:
            if evento.type == MOUSEMOTION:
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        elemento.actualizarColor(self.menu.screen, GestorRecursos.RED)
                    else:
                        elemento.actualizarColor(self.menu.screen, GestorRecursos.WHITE)
            if evento.type == MOUSEBUTTONDOWN:
                self.elementoClic = None
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        self.elementoClic = elemento
            if evento.type == MOUSEBUTTONUP:
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        if (elemento == self.elementoClic):
                            elemento.accion()

    def dibujar(self, pantalla):
        # Dibujamos primero la imagen de fondo
        pantalla.blit(self.imagen, self.imagen.get_rect())
        # Después las animaciones
        for animacion in self.animaciones:
            animacion.dibujar(pantalla)
        # Después los botones
        for elemento in self.elementosGUI:
            elemento.dibujar(pantalla)

#Pantalla concreta del menu. Es la única de este menú.
class PantallaPausaInicial(PantallaPausa):
    def __init__(self, menu):
        PantallaPausa.__init__(self, menu, 'pausa.jpg')

        # Creamos los botones/textos y los metemos en la lista
        textoPausaSombra = TextoPausaSombra(self)
        textoPausa = TextoPausa(self)
        textoJugar = TextoJugar(self)
        textoSalir = TextoSalir(self)
        textoMenu = TextoMenu(self)

        self.elementosGUI.append(textoPausaSombra)
        self.elementosGUI.append(textoPausa)
        self.elementosGUI.append(textoJugar)
        self.elementosGUI.append(textoSalir)
        self.elementosGUI.append(textoMenu)


# -------------------------------------------------
# Clase Pausa, la escena en sí, en Pygame

class Pausa(EscenaPygame):

    def __init__(self, director):

        self.transicion = True
        # Llamamos al constructor de la clase padre
        EscenaPygame.__init__(self, director);
        # Creamos la lista de pantallas
        self.listaPantallas = []
        # Creamos las pantallas que vamos a tener
        #   y las metemos en la lista
        # Es una pantalla única en este caso 
        self.listaPantallas.append(PantallaPausaInicial(self))
        # En que pantalla estamos actualmente
        self.mostrarPantallaInicial()

    def update(self, *args):
        return

    def eventos(self, lista_eventos):
        # Se mira si se quiere salir de esta escena
        for evento in lista_eventos:
            # Si se quiere salir, se le indica al director
            if evento.type == KEYDOWN:
                if evento.key == K_ESCAPE:
                    self.salirPrograma()
            elif evento.type == pygame.QUIT:
                self.director.salirPrograma()

        # Se pasa la lista de eventos a la pantalla actual
        self.listaPantallas[self.pantallaActual].eventos(lista_eventos)

    def dibujar(self, pantalla):
        self.listaPantallas[self.pantallaActual].dibujar(pantalla)

    #--------------------------------------
    # Metodos propios del menu de pausa

    # Salir del programa desde el menu de pausa, indicándoselo al director.
    def salirPrograma(self):
        self.director.salirPrograma()

    # Continuar el juego desde el menú de pausa.
    def seguirJuego(self):

        self.director.printPila() #Estado de la pila interna se informa por consola
        self.director.salirEscena() #Desapilamos la escena de Pausa y volvemos a la fase en la que estábamos  
        self.director.cimaPila().volver() #Se le indica a la escena que vuelve a estar en funcionamiento



    def irMenu(self):

        #Vacíamos pila por si viene de la pantalla de Pausa, ya que después desde menú se reapilan todas las fases y cinemática
        #De esta forma nos aseguramos que no se apilen escenas de más si mueres varias veces durante la ejecución del juego
        #Es un limpiado de pila por ahorro de memoria.

        #Dejamos una escena residual para evitar que salga antes de cambiar al menú (cambiarEscena)

        while (not self.director.pilaUnaEscena()):
            self.director.salirEscena()

        escena = menu.Menu(self.director) 

        #Cambiamos la escena residual por el menú. El ya se encarga de volver a restablecer la pila como al principio llamando al director
        #Al clickar en "Jugar", método acción.
        self.director.cambiarEscena(escena)

    def mostrarPantallaInicial(self):
        self.pantallaActual = 0
