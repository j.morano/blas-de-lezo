# -*- encoding: utf-8 -*-

import pygame
from escena import *
from gestorRecursos import *

from GUI import PantallaCinematica, Parrafo, Boton, BotonAtras, BotonSiguiente


################################################################################

PUNTUACION = 0

class BotonFinalizar(Boton):
    ''' Boton para terminar el juego '''
    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, 'FINALIZAR', GestorRecursos.font, 40, GestorRecursos.WHITE, (500, 550))


    def accion(self):
        self.pantalla.escenaCinematica.salirJuego()



class PantallaCinematicaFinalPrimera(PantallaCinematica):
    ''' Primera pantalla de la cinematica final '''

    def __init__(self, escenaCinematica):

        # Se carga el fondo
        PantallaCinematica.__init__(self, escenaCinematica, "cinematicafinal1final.jpg")

        # Se crea y añade el boton siguiente a la lista de elementosGUI
        botonSiguiente = BotonSiguiente(self)
        self.elementosGUI.append(botonSiguiente)

        # Se crea el texto de la cinematica y se añade como parrafo
        texto_cinematica = ["Tras la dura batalla con el alcaide", \
                            "John William, Blas de Lezo consigue", \
                            "su victoria y huye de la cárcel en busca de", \
                            "venganza."]
        Parrafo.escribirParrafo(self, texto_cinematica)



class PantallaCinematicaFinalSegunda(PantallaCinematica):
    ''' Segunda pantalla de la cinematica final '''

    def __init__(self, escenaCinematica):

        # Se carga el fondo
        PantallaCinematica.__init__(self, escenaCinematica, "cinematicafinal2final.jpg")

        # Se crea y añade el boton siguiente a la lista de elementosGUI
        botonAtras = BotonAtras(self)
        botonSiguiente = BotonSiguiente(self) 
        self.elementosGUI.append(botonSiguiente)
        self.elementosGUI.append(botonAtras)

        # Se crea el texto de la cinematica y se añade como parrafo
        texto_cinematica = ["Su siguiente objetivo es el almirante", \
                            "Edward Vernon, quien lo había capturado", \
                            "en Cartagena de Indias, y con quien", \
                            "disputará otra increíble batalla."]
        Parrafo.escribirParrafo(self, texto_cinematica)



class PantallaCinematicaFinalTercera(PantallaCinematica):
    ''' Tercera pantalla de la cinematica final '''

    def __init__(self, escenaCinematica):

        # Se carga el fondo
        PantallaCinematica.__init__(self, escenaCinematica, "cinematicafinal3final.jpg")

        # Se crea y añade el boton siguiente a la lista de elementosGUI
        botonAtras = BotonAtras(self)
        botonFinalizar = BotonFinalizar(self) 
        self.elementosGUI.append(botonFinalizar)
        self.elementosGUI.append(botonAtras)

        # Se crea el texto de la cinematica y se añade como parrafo
        # Además se imprime la puntuación que se ha logrado
        texto_cinematica = ["Después de vengarse de Edward Vernon", \
                            "volverá al Virreinato donde será", \
                            "recibido con todos los honores.",
                            "",
                            "",
                            "Tu puntuación:    "+str(GestorRecursos.recuperarPuntuacion())]
        Parrafo.escribirParrafo(self, texto_cinematica)



class EscenaCinematicaFinal(EscenaPygame):
    ''' Escena pygame de la cinemática final '''

    def __init__(self, director):

        EscenaPygame.__init__(self, director)

        # Para efectos en la primera pantalla de la cinemática
        self.primero = True

        # Para saber si se ha transicionado hacia la siguiente escena
        self.transicion = True

        # Creamos las pantallas y las introducimos en la lista. Inicializamos un
        # indicador de la pantalla actual y mostramos esa pantalla
        self.listaPantallas = []
        self.listaPantallas.append(PantallaCinematicaFinalPrimera(self))
        self.listaPantallas.append(PantallaCinematicaFinalSegunda(self))
        self.pantallaActual = 0
        self.mostrarPantallaInicial()


    def update(self, *args):
        ''' Como cada pantalla es estática, no se actualiza nada '''
        return


    def eventos(self, lista_eventos):
        ''' Metodo para tratar los eventos de esta escena '''

        # Eventos para salir del programa
        for evento in lista_eventos:
            if evento.type == KEYDOWN:
                if evento.key == K_ESCAPE:
                    self.salirPrograma()
            elif evento.type == pygame.QUIT:
                self.director.salirPrograma()

        # Se delega el resto de eventos a las pantallas concretas
        self.listaPantallas[self.pantallaActual].eventos(lista_eventos)


    def dibujar(self, pantalla):
        if (self.pantallaActual) == 0 and self.primero:
            self.primero = False
            GestorRecursos.BajarMusica(1000)
            pantalla.fill(GestorRecursos.BLACK)
            pygame.display.update()
            GestorRecursos.CargarMusica('CinematicaFinal.mp3', 0.35, -1)
            pygame.time.delay(1000)
        self.listaPantallas[self.pantallaActual].dibujar(pantalla)


    def avanzarCinematica(self):
        ''' Metodo para avanzar a la siguiente cinematica '''

        # Si es la última pantalla de la cinemática se inicia la siguiente
        # escena
        if self.pantallaActual >= (len(self.listaPantallas)-1):

            # Si se ha transicionado anteriormente ya no se hace nada
            if self.transicion:
                self.director.salirEscena()
                self.transicion = False


        # Sino es la última pantalla, se pasa a la siguiente pantalla de la
        # cinemática
        else:
            self.pantallaActual+=1
            if self.pantallaActual == (len(self.listaPantallas)-1):
                self.listaPantallas.append(PantallaCinematicaFinalTercera(self))


    def salirJuego(self):
        ''' Metodo para salir al menu del juego '''

        self.director.salirEscena()
        GestorRecursos.pararMusica()
        GestorRecursos.CargarMusica('PirateIntroMenu.mp3', 0.5, -1)


    def retrasarCinematica(self):
        ''' Metodo para ir hacia la pantalla anterior en la cinemática '''

        self.pantallaActual-=1


    def mostrarPantallaInicial(self):
        ''' Metodo para situar la pantalla inicial en la primera '''

        self.pantallaActual = 0