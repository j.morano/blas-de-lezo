# -*- coding: utf-8 -*-

import pygame, sys, os
from pygame import *
from os import path

import json

NUMERO_CANALES = 10


class GestorRecursos(object):
    ''' Clase para gestionar los recursos de sonido, imagenes, ... '''

    # Inicializamos los paths
    img_dir = path.join(path.dirname(__file__), 'IMAGENES')
    coord_dir = path.join(path.dirname(__file__), 'COORDENADAS')
    sound_dir = path.join(path.dirname(__file__), 'AUDIOS-EFECTOS')
    music_dir = path.join(path.dirname(__file__), 'MUSICA')
    font = "FUENTES/immortal.ttf"

    # Inicializamos los diccionarios de recursos
    recursos = {}
    recursosSonido = {}

    # Para solucionar problemas de retardo en la reproducción de sonidos
    pygame.mixer.pre_init(44100, -16, 2, 2048)
    pygame.mixer.init()

    # Canales para los sonidos
    pygame.mixer.set_num_channels(NUMERO_CANALES)
    recursosCanales = [pygame.mixer.Channel(i) for i in range(NUMERO_CANALES)]


    # Colores
    BLACK = (0,0,0)
    WHITE = (255,255,255)
    GREENYELLOW = (143,245,34)
    YELLOW = (234, 245, 34)
    GREY = (210,210,210)
    GRAY = (20,20,20)
    DARKGREY = (93,94,94)
    RED = (255,0,0)
    GREEN = (0,255,0)
    REDORANGE = (245,103,32)
    ORANGE = (255,140,0)
    LIGHTBLUE = (0, 246, 255)
    BLUE = (0, 148, 255)
    DARKBLUE = (0, 89, 255)


    # Tamaño de ventana
    WINDOWWIDTH = 800
    WINDOWHEIGHT = 600

    # Puntuación del jugador
    puntuacionJugador = 0


    @classmethod
    def CargarImagen(cls, nombre, escalar, colorkey=None, convert=True, escalado=(WINDOWWIDTH, WINDOWHEIGHT)):
        ''' Método estático para cargar una imagen y escalarla '''

        # Si la imagen está cargada
        if nombre in cls.recursos:
            imagen = cls.recursos[nombre]

        # Si la imagen no está cargada
        else:
            # Concatenación de path, cargado, convertido y uso de colorkey
            rutacompleta=path.join(cls.img_dir, nombre)
            try:
                imagen = pygame.image.load(rutacompleta)
            except pygame.error:
                print('Error al cargar la imagen:', rutacompleta)
                raise SystemExit
            if convert:
                imagen = imagen.convert()
            if colorkey is not None:
                if colorkey is -1:
                    colorkey = imagen.get_at((0,0))
                imagen.set_colorkey(colorkey, RLEACCEL)

            # Almacenamiento en los recursos
            cls.recursos[nombre] = imagen

        # Escalado
        if escalar:
            imagen = pygame.transform.scale(imagen, escalado)
        # Se devuelve
        return (imagen, imagen.get_rect())


    @classmethod
    def CargarMusica(cls,nombre, volumen, tiempo):
        ''' Método estático para cargar música '''

        pygame.mixer.music.set_volume(volumen)
        pygame.mixer.music.load(path.join(cls.music_dir, nombre))
        pygame.mixer.music.play(tiempo)
        return


    @classmethod
    def BajarMusica(cls, out):
        ''' Método estático para realizar un fadeout '''

        pygame.mixer.music.fadeout(out)
        return


    @classmethod
    def pararMusica(cls):
        ''' Método estático para parar la música '''

        pygame.mixer.music.stop()


    @classmethod
    def CargarEfectoSonido(cls,nombre,volumen, gestionarCanal = False, numChannel = 0, fadeout=None):
        ''' Método estático para cargar un efecto de sonido '''

        # Si el efecto está cargado
        if nombre in cls.recursosSonido:
            sonido = cls.recursosSonido[nombre]

        # Si el efecto no está cargado
        else:
            # Concatenación de path y cargado
            rutacompleta=path.join(cls.sound_dir, nombre)
            try:
                sonido = pygame.mixer.Sound(rutacompleta)
                cls.recursosSonido[nombre] = sonido
            except pygame.error:
                print('Error al cargar el efecto de sonido:', rutacompleta)
                raise SystemExit

        # Regular el volumen
        sonido.set_volume(volumen)

        # Si se gestiona canales de audio
        if (gestionarCanal):
            canal = cls.recursosCanales[numChannel]
            if not canal.get_busy():
                canal.play(cls.recursosSonido[nombre])
        # Sino se gestiona canales de audio
        else:
            sonido.play()

        # Si se quiere hacer un fadeout
        if fadeout is not None:
            sonido.fadeout(fadeout)


    @classmethod
    def CargarArchivoCoordenadas(cls, nombre):
        ''' Método estático para cargar un archivo de coordenadas '''

        # Si el nombre de archivo está entre los recursos ya cargados
        if nombre in cls.recursos:
            return cls.recursos[nombre]

        # Si no ha sido cargado anteriormente
        else:
            # Se carga el recurso indicando el nombre de su carpeta
            fullname = path.join(cls.coord_dir, nombre)
            pfile=open(fullname,'r')
            datos=pfile.read()
            pfile.close()
            # Se almacena
            cls.recursos[nombre] = datos

            return datos


    @classmethod
    def text_format(cls, message, textFont, textSize, textColor):
        ''' Método estático para formateo de texto '''

        pygame.font.init()
        newFont=pygame.font.Font(textFont, textSize)
        newText=newFont.render(message, 0, textColor)
        return newText


    @classmethod
    def text_format_shadow(cls, message, textFont, textSize, textColor, shadowColor):
        ''' Método estático para formateo de texto con sombra '''

        pygame.font.init()
        newFont=pygame.font.Font(textFont, textSize)
        newText=newFont.render(message, 0, textColor)
        newShadow=newFont.render(message, True, shadowColor)
        return (newText, newShadow)


    @classmethod
    def guardarEstado(cls, puntuacion, fase):
        ''' Método estático para guardar la puntuación del jugador en una fase
            en el fichero'''
        cls.puntuacionJugador = puntuacion
        with open('PARTIDAS/partida.json', 'w') as outfile:
            json.dump({'fase': fase+1, 'puntuacion': puntuacion}, outfile)


    @classmethod
    def guardarPuntuacion(cls, puntuacion):
        ''' Método estático para almacenar la puntuación '''
        cls.puntuacionJugador = puntuacion


    @classmethod
    def recuperarPuntuacion(cls):
        ''' Método estático para recuperar la puntuación '''
        return cls.puntuacionJugador