# -*- coding: utf-8 -*-
#!/usr/bin/python3

from director import Director
from menu import Menu


#Ejecucion principal del juego.
if __name__ == '__main__':

	#Se toma la instancia única del director (singleton)
    director = Director.getInstanceDirector()

    #Se crea el menu principal, donde se gestionarán el resto de escenas en la pila.
    escena = Menu(director)
    director.apilarEscena(escena)
    
    #Iniciamos la ejecución para eventos, actualizaciones, dibujados...En el bucle de eventos.
    director.ejecutar()
