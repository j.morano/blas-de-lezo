# -*- coding: utf-8 -*-

import pygame
from pygame.locals import *
from escena import *
from gestorRecursos import *
from gameover import GameOverMenu

import random

import time


################################################################################


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# Constantes
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

# Movimientos del personaje
QUIETO = 0
IZQUIERDA = 1
DERECHA = 2
ARRIBA = 3
ABAJO = 4

#Posturas de estado de los sprites
SPRITE_QUIETO = 0
SPRITE_ANDANDO = 1
SPRITE_SALTANDO = 2
SPRITE_ATACANDO = 3
SPRITE_MURIENDO = 4


# Velocidades de los distintos personajes
VELOCIDAD_JUGADOR = 0.2 # Pixeles por milisegundo
VELOCIDAD_SALTO_JUGADOR = 0.3 # Pixeles por milisegundo
RETARDO_ANIMACION_JUGADOR = 4 # updates que durará cada imagen del personaje
                              # debería de ser un valor distinto para cada postura

RETARDO_MUERTE = 0.75 #Tiempo de los sprites de enemigos para parpadeo durante su muerte 

VELOCIDAD_CARCELERO = 0.12 # Pixeles por milisegundo
VELOCIDAD_SALTO_CARCELERO = 0.27 # Pixeles por milisegundo
RETARDO_ANIMACION_CARCELERO = 5 # updates que durará cada imagen del personaje
                             # debería de ser un valor distinto para cada postura
# El CARCELERO camina un poco más lento que el jugador, y salta menos

GRAVEDAD = 0.0004 # Píxeles / ms2
DANO = 0.05 #Dano base para las colisiones con plataformas o enemigos (se ajusta con coeficientes en cada caso)



# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# Clases de los objetos del juego
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------



# ------------------------------------------------------------------------------
# Clase MiSprite
# 
#   - Implementa el comportamiento genérico de los sprites
#


class MiSprite(pygame.sprite.Sprite):
    "Los Sprites que tendra este juego"
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.posicion = (0, 0)
        self.velocidad = (0, 0)
        self.scroll   = (0, 0)

    def establecerPosicion(self, posicion):
        self.posicion = posicion
        self.rect.left = self.posicion[0] - self.scroll[0]
        self.rect.bottom = self.posicion[1] - self.scroll[1]

    def establecerPosicionPantalla(self, scrollDecorado):
        self.scroll = scrollDecorado;
        (scrollx, scrolly) = self.scroll;
        (posx, posy) = self.posicion;
        self.rect.left = posx - scrollx;
        self.rect.bottom = posy - scrolly;

    def incrementarPosicion(self, incremento):
        (posx, posy) = self.posicion
        (incrementox, incrementoy) = incremento
        self.establecerPosicion((posx+incrementox, posy+incrementoy))

    def update(self, tiempo):
        incrementox = self.velocidad[0]*tiempo
        incrementoy = self.velocidad[1]*tiempo
        # Si hay algún incremento, incrementar posición
        if incrementox != 0 or incrementoy != 0:
            self.incrementarPosicion((incrementox, incrementoy))




# ------------------------------------------------------------------------------
# Clase Personaje
# 
#   - De la que heredan todos los personajes del juego
#

class Personaje(MiSprite):
    "Cualquier personaje del juego"

    # Parámetros pasados al constructor de esta clase:
    #  Archivo con la hoja de Sprites
    #  Archivo con las coordenadoas dentro de la hoja
    #  Numero de imagenes en cada postura
    #  Velocidad de caminar y de salto
    #  Retardo para mostrar la animacion del personaje
    def __init__(self, archivoImagen, archivoCoordenadas, numImagenes, velocidadCarrera, velocidadSalto, retardoAnimacion):

        # Primero invocamos al constructor de la clase padre
        MiSprite.__init__(self);

        # self.llave = False

        #Controlar si en la animación de ataque ya ha consumido su ataque (evitar eliminaciones múltiples por acumulación de colisiones)
        self.ataqueGastado = False

        #Vida por defecto de los carceleros comunes
        self.vidas = 1

        # Se carga la hoja
        self.hoja,_ = GestorRecursos.CargarImagen(archivoImagen,False,-1)
        self.hoja = self.hoja.convert_alpha()
        
        # El movimiento que esta realizando
        self.movimiento = QUIETO
        # Lado hacia el que esta mirando
        self.mirando = IZQUIERDA

        # Leemos las coordenadas de un archivo de texto
        datos = GestorRecursos.CargarArchivoCoordenadas(archivoCoordenadas)
        datos = datos.split()
        self.numPostura = 1;
        self.numImagenPostura = 0;
        cont = 0;
        self.coordenadasHoja = [];
        for linea in range(0, len(numImagenes)):
            self.coordenadasHoja.append([])
            tmp = self.coordenadasHoja[linea]
            for postura in range(1, numImagenes[linea]+1):
                tmp.append(pygame.Rect((int(datos[cont]), int(datos[cont+1])), (int(datos[cont+2]), int(datos[cont+3]))))
                cont += 4

        # El retardo a la hora de cambiar la imagen del Sprite (para que no se mueva demasiado rápido)
        self.retardoMovimiento = 0;

        # En que postura esta inicialmente
        self.estado = QUIETO
        self.numPostura = QUIETO

        # El rectangulo del Sprite
        self.rect = pygame.Rect(100,100,self.coordenadasHoja[self.numPostura][self.numImagenPostura][2],self.coordenadasHoja[self.numPostura][self.numImagenPostura][3])

        # Las velocidades de caminar y salto
        self.velocidadCarrera = velocidadCarrera
        self.velocidadSalto = velocidadSalto

        # El retardo en la animacion del personaje (podria y deberia ser distinto para cada postura)
        self.retardoAnimacion = retardoAnimacion

        #Comprobar si se encuentra en el estado de "muriendo" para efectuar los parpadeos de la eliminación
        self.muriendo = False

        # Y actualizamos la postura del Sprite inicial, llamando al metodo correspondiente
        self.actualizarPostura()


    # Método base para realizar el movimiento: simplemente se le indica cual va a hacer, y lo almacena
    def mover(self, movimiento):

        self.movimiento = movimiento


    def actualizarPostura(self):

    	#controlar el tipo de postura atendiendo si no está atacando
    	#Se separa del resto de movimientos, de forma que "ATACANDO" puede estar activo en combinación con uno del resto (y solo uno)

        if self.numPostura != SPRITE_ATACANDO:
            
            if self.estado == SPRITE_ANDANDO:
                self.numPostura = SPRITE_ANDANDO
            elif self.estado == SPRITE_SALTANDO:
                self.numPostura = SPRITE_SALTANDO
            elif self.estado == SPRITE_QUIETO:
                self.numPostura = SPRITE_QUIETO


        #Si está muriendo, control del numero de iteraciones en las que está presente un parpadeo de muerte
        if self.muriendo:
            self.numPostura = SPRITE_MURIENDO
            self.retardoMovimiento -= RETARDO_MUERTE #En el de muerte se mantiene durante más tiempo.
        else:
            self.retardoMovimiento -= 1 #En el resto de movimientos el cambio es algo más rápido

        # Miramos si ha pasado el retardo para dibujar una nueva postura
        if (self.retardoMovimiento < 0):
            self.retardoMovimiento = self.retardoAnimacion
            # Si ha pasado, actualizamos la postura
            self.numImagenPostura += 1
            if self.numImagenPostura >= len(self.coordenadasHoja[self.numPostura]):
                if self.numPostura == SPRITE_ATACANDO:
                    self.numPostura = SPRITE_QUIETO
                    self.ataqueGastado = False #El ataque vuelve a estar disponible tras terminar el retardo de la animación de combate.
                if self.numPostura == SPRITE_MURIENDO:
                    self.vidas -= 1 #Si estaba muriendo el enemigo, a este se le resta la vida
                    if self.vidas == 0:
                        self.kill() #Si ya no tiene vidas, eliminamos el sprite del grupo
                    else:
                        self.muriendo = False #Para el caso de enemigos con más de una vida, vuelve al estado quieto a la "espera" de otro golpe
                        self.numPostura = SPRITE_QUIETO #Tras "perder" esa vida, deja de hacer el estado "muriendo" y vuelve a uno normal.
                self.numImagenPostura = 0
            if self.numImagenPostura < 0:
                self.numImagenPostura = len(self.coordenadasHoja[self.numPostura])-1
            self.image = self.hoja.subsurface(self.coordenadasHoja[self.numPostura][self.numImagenPostura])

            # Si esta mirando a la izquiera, cogemos la porcion de la hoja
            if self.mirando == IZQUIERDA:
                self.image = self.hoja.subsurface(self.coordenadasHoja[self.numPostura][self.numImagenPostura])
            #  Si no, si mira a la derecha, invertimos esa imagen
            elif self.mirando == DERECHA:
                self.image = pygame.transform.flip(self.hoja.subsurface(self.coordenadasHoja[self.numPostura][self.numImagenPostura]), 1, 0)

            # Alineamiento adecuado de los sprites independientemente de
            # si tienen dimensiones distintas

            # Evitar pequeños desplazamientos de los sprites en las actualizaciones de los movimientos

            if self.mirando == DERECHA:
                left = self.rect.left
                bottom = self.rect.bottom
                self.rect = self.image.get_rect()
                self.rect.bottom = bottom
                self.rect.left = left
            else:
                right = self.rect.right
                bottom = self.rect.bottom
                self.rect = self.image.get_rect()
                self.rect.bottom = bottom
                self.rect.right = right
            



    def update(self, grupoPlataformas, grupoPlataformasColision, grupoPlataformasPinchos, grupoPlataformasFuego, grupoPlataformasEscaleras, director, tiempo):


        # Las velocidades a las que iba hasta este momento
        (velocidadx, velocidady) = self.velocidad

        if self.muriendo:

        	#Si está muriendo, deja de "perseguir" al jugador
            (velocidadx, velocidady) = (0,0)

            # Actualizamos la imagen a mostrar
            self.actualizarPostura()

            # Aplicamos la velocidad en cada eje      
            self.velocidad = (velocidadx, velocidady)

            # Y llamamos al método de la superclase para que, según la velocidad y el tiempo
            #  calcule la nueva posición del Sprite
            MiSprite.update(self, tiempo)
            
            return


        # Si vamos a la izquierda o a la derecha        
        if (self.movimiento == IZQUIERDA) or (self.movimiento == DERECHA):
            # Esta mirando hacia ese lado
            self.mirando = self.movimiento

            # Si vamos a la izquierda, le ponemos velocidad en esa dirección
            if self.movimiento == IZQUIERDA:
                velocidadx = -self.velocidadCarrera
            # Si vamos a la derecha, le ponemos velocidad en esa dirección
            else:
                velocidadx = self.velocidadCarrera

            # Si no estamos en el aire
            if self.estado != SPRITE_SALTANDO:
                # La postura actual sera estar caminando
                self.estado = SPRITE_ANDANDO
                # Ademas, si no estamos encima de ninguna plataforma, caeremos
                if pygame.sprite.spritecollideany(self, grupoPlataformas) == None:
                    self.estado = SPRITE_SALTANDO
                # Si se colisiona con alguna escalera, subir escaleras de forma
                # automática al desplazarse horizontalmente.
                plataformaesc = pygame.sprite.spritecollideany(self, grupoPlataformasEscaleras)
                if plataformaesc != None:
                     self.establecerPosicion((self.posicion[0], plataformaesc.posicion[1]-plataformaesc.rect.height+1))


        # Si queremos saltar
        elif self.movimiento == ARRIBA:
            # La postura actual sera estar saltando
            self.estado = SPRITE_SALTANDO
            # Le imprimimos una velocidad en el eje y
            if velocidady == 0:
                velocidady = -self.velocidadSalto

        # Si no se ha pulsado ninguna tecla
        elif self.movimiento == QUIETO:
            # Si no estamos saltando, la postura actual será estar quieto
            if not self.estado == SPRITE_SALTANDO:
                self.estado = SPRITE_QUIETO
            velocidadx = 0


        # Además, si estamos en el aire
        if self.estado == SPRITE_SALTANDO:

            # Miramos a ver si hay que parar de caer: si hemos llegado a una plataforma
            #  Para ello, miramos si hay colision con alguna plataforma del grupo
            plataforma = pygame.sprite.spritecollideany(self, grupoPlataformas)
            #  Ademas, esa colision solo nos interesa cuando estamos cayendo
            #  y solo es efectiva cuando caemos encima, no de lado, es decir,
            #  cuando nuestra posicion inferior esta por encima de la parte de abajo de la plataforma
            if (plataforma != None) and (velocidady>0) and (plataforma.rect.bottom>self.rect.bottom):
                # Lo situamos con la parte de abajo un pixel colisionando con la plataforma
                #  para poder detectar cuando se cae de ella
                self.establecerPosicion((self.posicion[0], plataforma.posicion[1]-plataforma.rect.height+1))
                # Lo ponemos como quieto
                self.estado = SPRITE_QUIETO
                # Y estará quieto en el eje y
                velocidady = 0

            # Si no caemos en una plataforma, aplicamos el efecto de la gravedad
            else:
                velocidady += GRAVEDAD * tiempo

    
        pinchos = pygame.sprite.spritecollideany(self, grupoPlataformasPinchos)
        fuego = pygame.sprite.spritecollideany(self, grupoPlataformasFuego)

        if isinstance(self, Jugador):
            
            if (pinchos != None): #Si contacta con los pinchos, efectúa acciones correspondientes
                GestorRecursos.CargarEfectoSonido("danoPinchos.wav", 0.15, True, 1)
                self.setHealth(self.getHealth()-DANO*tiempo, director)

            if (fuego != None): #Si contacta con la lava, efectúa acciones correspondientes (efectúa más daño)
                GestorRecursos.CargarEfectoSonido("lava.wav", 0.25, True, 1)
                GestorRecursos.CargarEfectoSonido("danoPinchos.wav", 0.15, True, 2)
                self.setHealth(self.getHealth()-(DANO*2)*tiempo, director)

            #Caidas al vacío, muerte
            if (self.rect.top > GestorRecursos.WINDOWHEIGHT):
            	escena = GameOverMenu(director)
            	director.apilarEscena(escena)
        else:
        	#Si son enemigos

        	#Muerte por contacto con la lava (enemigos)
            if (fuego != None): 
                self.morir()

            #Eliminacion del enemigo por caída al vacío
            if (self.rect.top > GestorRecursos.WINDOWHEIGHT):
                self.kill()


        # Actualizamos la imagen a mostrar
        self.actualizarPostura()

        # Aplicamos la velocidad en cada eje      
        self.velocidad = (velocidadx, velocidady)

        # Y llamamos al método de la superclase para que, según la velocidad y el tiempo
        #  calcule la nueva posición del Sprite
        MiSprite.update(self, tiempo)
        
        return

    def morir(self):
        self.muriendo = True


    def estaMuriendo(self):
        return self.muriendo


    def setVelocidad(self, vx=None, vy=None):
        (vx_nuevo, vy_nuevo) = self.velocidad
        if vx is not None:
            vx_nuevo = vx
        if vy is not None:
            vy_nuevo = vy
        self.velocidad = (vx_nuevo, vy_nuevo)


    def getVelocidad(self):
        return self.velocidad


    def quieto(self):
        # Indicamos la acción a realizar segun la tecla pulsada para el jugador
        Personaje.mover(self,QUIETO)


    def estaQuieto(self):
        return self.estado == QUIETO


    def atacar(self):
        self.numPostura = SPRITE_ATACANDO
        self.numImagenPostura = 0


    def estaAtacando(self):
        return self.numPostura == SPRITE_ATACANDO

    def miraHacia(self):
        return self.mirando

    def puedeAtacar(self): #comprobar si puede consumir el ataque de la animación
        return not self.ataqueGastado

    def gastarAtaque(self):
        self.ataqueGastado = True




# ------------------------------------------------------------------------------
# Clase Jugador
# 
#   - De la que heredan todos los personajes jugadores
#


class Jugador(Personaje):
    "Cualquier personaje jugador del juego"
    def __init__(self):
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        Personaje.__init__(self,'jugador_carcelero.png','coord_jugador.txt', [10, 6, 9, 4], VELOCIDAD_JUGADOR, VELOCIDAD_SALTO_JUGADOR, RETARDO_ANIMACION_JUGADOR);
        self.health = 100 #Inicializamos la vida al maximo en cada fase.
        self.saliendo = False #Para evitar que se apile más de una vez el menu de muerte ante el bucle de eventos.
        self.momentoUltimoAtaque = 0
        self.intervaloAtaques = 0.5


    def mover(self, teclasPulsadas, arriba, abajo, izquierda, derecha):
        # Indicamos la acción a realizar segun la tecla pulsada para el jugador
        if teclasPulsadas[arriba]:
            Personaje.mover(self,ARRIBA)
        elif teclasPulsadas[izquierda]:
            Personaje.mover(self,IZQUIERDA)
        elif teclasPulsadas[derecha]:
            Personaje.mover(self,DERECHA)
        else:
            Personaje.mover(self,QUIETO)


    def checkAttack(self, teclasPulsadas, espacio):
        if teclasPulsadas[espacio]:

            if abs(time.time() - self.momentoUltimoAtaque) > self.intervaloAtaques:
                self.momentoUltimoAtaque = time.time()
            	#Capturamos por otro lado de forma independiente la pulsación para el ataque.
                self.numPostura = SPRITE_ATACANDO
                self.numImagenPostura = 0
                GestorRecursos.CargarEfectoSonido("Espada.wav", 0.03, True, 6)            


    def setHealth(self, currentHealth, director):
        if (self.health > 0):
            self.health=currentHealth
        else: #Si se ha consumido la salud en la barra
            self.health=0
            if not self.saliendo: #Si no se está apilando otro menú previamente
                escena = GameOverMenu(director) #Menu de muerte creado
                director.apilarEscena(escena) #Apilamos para tener el control sobre la fase en la que estábamos
                self.saliendo = True #Flag de control


    def getHealth(self):
        return self.health


    def getPorcIntervalo(self):
        porcentaje = (abs(time.time() - self.momentoUltimoAtaque)/self.intervaloAtaques)*100
        if porcentaje < 100:
            return porcentaje
        else:
            return 100




# ------------------------------------------------------------------------------
# Clase NoJugador
# 
#   - De la que heredan todos los personajes no jugadores (la máquina)
#

class NoJugador(Personaje):
    "El resto de personajes no jugadores"
    def __init__(self, archivoImagen, archivoCoordenadas, numImagenes, velocidad, velocidadSalto, retardoAnimacion):
        # Primero invocamos al constructor de la clase padre con los parámetros pasados
        Personaje.__init__(self, archivoImagen, archivoCoordenadas, numImagenes, velocidad, velocidadSalto, retardoAnimacion);

    # Implementado en cada enemigo concreto
    def mover_cpu(self, jugador1):
        # Por defecto un enemigo no hace nada
        return

    def morir(self):
        GestorRecursos.CargarEfectoSonido("carcelero0.wav", 0.2, True, 5)
        self.muriendo = True
        self.numImagenPostura = 0




# ------------------------------------------------------------------------------
# Clase JefeFinal
# 
#   - Alcaide de la prisió, con movimientos más sofisticados y mayor
#     rapidez
#

class JefeFinal(NoJugador):
    "El enemigo 'Jefe Final'"
    def __init__(self):
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        # Presenta más velocidad que el propio protagonista
        NoJugador.__init__(self,'jefe_final_grande.png','coord_jefe_grande.txt', [12, 6, 12, 1, 11], VELOCIDAD_JUGADOR*1.2, VELOCIDAD_SALTO_CARCELERO, RETARDO_ANIMACION_CARCELERO);
        self.rand = -1 #Elección aleatoria de como bajar una plataforma
        self.vidas = 7 #Numero total de vidas en el combate contra el jugador
        self.tiempoGolpe = 0

    # La implementacion de la inteligencia segun este personaje particular (jefe final)
    def mover_cpu(self, jugador1):

        # Movemos solo a los enemigos que esten en la pantalla
        if self.rect.right>0 and self.rect.left<ANCHO_PANTALLA and self.rect.top>0 \
                and self.rect.bottom<ALTO_PANTALLA and not self.muriendo:

            # Miramos cual es el jugador mas cercano
            jugadorMasCercano = jugador1
            # Y nos movemos andando hacia el
            x_jugador, y_jugador = jugadorMasCercano.posicion
            x, y = self.posicion
            
            margen = 60 # margen de control para fijación del jugador y su seguimiento

            """ El funcionamiento se basa en comprobar margen del jugador en horizontal y vertical
            El jefe final actúa dependiendo de las condiciones con esos margenes:

            	- Si se encuentra debajo de los margenes verticales dentro de los horizontales salta a por el jugador 
            	- Si se encuentra por encima de los margenes verticales dentro de los horizontales, elige una direccion de forma
            		aleatoria y baja por la plataforma
            	- Si se no se encuentra en ninguno, persecución en el eje X """

            if self.rand == -1:
                if x_jugador - x < -margen:
                    Personaje.mover(self,IZQUIERDA)
                elif x_jugador - x > margen:
                    Personaje.mover(self,DERECHA)

                if y_jugador - y < -margen and abs(x_jugador-x)<=margen:
                    Personaje.mover(self,ARRIBA)

                if y_jugador - y > margen and abs(x_jugador-x)<=margen:
                    self.rand = random.randint(0,1)
                    if self.rand == 1:
                        Personaje.mover(self,DERECHA)
                    else:
                        Personaje.mover(self,IZQUIERDA)

                if random.random()<0.04:
                    Personaje.mover(self,ARRIBA)


            # Si consigue ponerse al mismo nivel, permitir nuevos movimientos
            if abs(y_jugador - y) <= margen:
                self.rand = -1

        # Si este personaje no está en pantalla, no hará nada
        else:
            Personaje.mover(self,QUIETO)
            self.rand = -1

    # "3" es el tiempo de gracia durante el que es inmune después de recibir un golpe
    def morir(self): # Muerte personalizada del jefe final
        if time.time()-self.tiempoGolpe > 3:
            GestorRecursos.CargarEfectoSonido("carcelero0.wav", 0.07, True, 5)
            self.muriendo = True
            self.numImagenPostura = 0
            self.tiempoGolpe = time.time()
            Personaje.mover(self,ARRIBA)

    # Comprueba si el jefe está en un momento de inmunidad
    def estaEscudado(self):
        return (time.time()-self.tiempoGolpe <= 3)


# ------------------------------------------------------------------------------
# Clase Carcelero
# 
#   - Carcelero que te daña por contacto y salta
#

class Carcelero(NoJugador):
    "El enemigo 'Carcelero'"
    def __init__(self):
        # Aplicamos cierta aleatoriedad en la velocidad de los carceleros
        upper_limit = 1.3
        lower_limit = 0.9
        correccion = random.random() * (upper_limit - lower_limit) + lower_limit  #valor concreto en el rango establecido
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        NoJugador.__init__(self,'jugador_carcelero.png','coord_carcelero.txt', [12, 6, 12, 1, 11], (VELOCIDAD_CARCELERO*correccion), VELOCIDAD_SALTO_CARCELERO, RETARDO_ANIMACION_CARCELERO);
        self.vidas = 2 # una vida adicional (el mínimo es 1)

    # La implementacion de la inteligencia segun este personaje particular (carcelero tipo 2)
    def mover_cpu(self, jugador1):

        # Movemos solo a los enemigos que esten en la pantalla
        if self.rect.left>0 and self.rect.right<ANCHO_PANTALLA and self.rect.bottom>0 \
                and self.rect.top<ALTO_PANTALLA and not self.muriendo:

            # Miramos cual es el jugador mas cercano
            jugadorMasCercano = jugador1
            # Y nos movemos andando hacia el
            x_jugador, y_jugador = jugadorMasCercano.posicion
            x, y = self.posicion
            
            margen = 50 #Margen de control del jugador

            if x_jugador - x < -margen:
                Personaje.mover(self,IZQUIERDA)
            elif x_jugador - x > margen:
                Personaje.mover(self,DERECHA)

            if y_jugador - y < -margen and abs(x_jugador-x)<=margen:
                Personaje.mover(self,ARRIBA) #Control del salto del enemigo si pasas por encima en un rango.

        # Si este personaje no está en pantalla, no hará nada
        else:
            Personaje.mover(self,QUIETO)




# ------------------------------------------------------------------------------
# Clase CarceleroSimple
# 
#   - Carcelero que te daña por contacto
#

class CarceleroSimple(NoJugador):
    "El enemigo 'Carcelero Simple'"
    def __init__(self):
        # Aplicamos cierta aleatoriedad en la velocidad de los carceleros
        upper_limit = 1.0
        lower_limit = 0.5
        correccion = random.random() * (upper_limit - lower_limit) + lower_limit #valor concreto en el rango establecido
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        NoJugador.__init__(self,'carcelero_simple.png','coord_carcelero_simple.txt', [12, 6, 12, 1, 11], (VELOCIDAD_CARCELERO*correccion), VELOCIDAD_SALTO_CARCELERO, RETARDO_ANIMACION_CARCELERO);

    # La implementación de la inteligencia segun este personaje particular (carcelero simple)
    def mover_cpu(self, jugador1):

        # Movemos solo a los enemigos que esten en la pantalla
        if self.rect.left>0 and self.rect.right<ANCHO_PANTALLA and self.rect.bottom>0 \
                and self.rect.top<ALTO_PANTALLA and not self.muriendo:

            # Miramos cual es el jugador más cercano
            jugadorMasCercano = jugador1        
            # Y nos movemos andando hacia el
            if jugadorMasCercano.posicion[0]<self.posicion[0]:
                Personaje.mover(self,IZQUIERDA)
            else:
                Personaje.mover(self,DERECHA)

        # Si este personaje no esta en pantalla, no hará nada
        else:
            Personaje.mover(self,QUIETO)




# ------------------------------------------------------------------------------
# Clase CarceleroPistolero
# 
#   - Carcelero que dispara a QUEMARROPA
#

class CarceleroPistolero(NoJugador):
    "El enemigo 'Carcelero'"
    def __init__(self):
        # Aplicamos cierta aleatoriedad en la velocidad de los carceleros
        upper_limit = 1.3
        lower_limit = 0.9
        correccion = random.random() * (upper_limit - lower_limit) + lower_limit  #valor concreto en el rango establecido
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        NoJugador.__init__(self,'carcelero_pistolero.png','coordCarcelero_pistolero.txt', [12, 6, 12, 8, 11], (VELOCIDAD_CARCELERO*correccion), VELOCIDAD_SALTO_CARCELERO, RETARDO_ANIMACION_CARCELERO);
        #self.vidas = 1


    # La implementacion de la inteligencia segun este personaje particular (carcelero tipo 2)
    def mover_cpu(self, jugador1):

        # Movemos solo a los enemigos que esten en la pantalla
        if self.rect.left>0 and self.rect.right<ANCHO_PANTALLA and self.rect.bottom>0 \
                and self.rect.top<ALTO_PANTALLA and not self.muriendo:

            # Miramos cual es el jugador mas cercano
            jugadorMasCercano = jugador1
            # Y nos movemos andando hacia el
            x_jugador, y_jugador = jugadorMasCercano.posicion
            x, y = self.posicion
            
            margen = 50 #Margen de control del jugador

            if not self.estaAtacando():
                if x_jugador - x < -margen:
                    Personaje.mover(self,IZQUIERDA)
                elif x_jugador - x > margen:
                    Personaje.mover(self,DERECHA)
                elif abs(x_jugador - x) <= margen and abs(y_jugador - y) <= margen:
                    self.atacar()
                    Personaje.mover(self,QUIETO)

        # Si este personaje no está en pantalla, no hará nada
        else:
            Personaje.mover(self,QUIETO)


    def morir(self):
        GestorRecursos.CargarEfectoSonido("carcelero0.wav", 0.07, True, 5)
        Personaje.mover(self,QUIETO)
        self.numImagenPostura = 0
        self.muriendo = True


    def estaAtacando(self):
        if self.numPostura == SPRITE_ATACANDO:
            # Comprueba si el sprite está atacando y si está justo en el momento
            # del disparo
            if self.numImagenPostura == 3 or self.numImagenPostura == 4:
                return True
        return False

    def atacar(self):
        if self.numPostura != SPRITE_ATACANDO:
            self.numPostura = SPRITE_ATACANDO
            self.numImagenPostura = 0
        GestorRecursos.CargarEfectoSonido("disparo_trabuco.wav", 0.1, True, 6)





# ------------------------------------------------------------------------------
# Clase CarceleroEspadachin
# 
#   - Carcelero que te ataca con un sable
#

class CarceleroEspadachin(NoJugador):
    "El enemigo 'Carcelero'"
    def __init__(self):
        # Aplicamos cierta aleatoriedad en la velocidad de los carceleros
        upper_limit = 1.1
        lower_limit = 0.8
        correccion = random.random() * (upper_limit - lower_limit) + lower_limit  #valor concreto en el rango establecido
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        NoJugador.__init__(self,'carcelero_espadachin.png','coordCarcelero_espadachin.txt', [6, 12, 1, 4, 11], (VELOCIDAD_CARCELERO*correccion), VELOCIDAD_SALTO_CARCELERO, RETARDO_ANIMACION_CARCELERO);
        #self.vidas = 1


    # La implementacion de la inteligencia segun este personaje particular (carcelero tipo 2)
    def mover_cpu(self, jugador1):
        # if self.muriendo:
        #     Personaje.mover(self,QUIETO)
        #     return
        # Movemos solo a los enemigos que esten en la pantalla
        if self.rect.left>0 and self.rect.right<ANCHO_PANTALLA and self.rect.bottom>0 \
                and self.rect.top<ALTO_PANTALLA and not self.muriendo:

            # Miramos cual es el jugador mas cercano
            jugadorMasCercano = jugador1
            # Y nos movemos andando hacia el
            x_jugador, y_jugador = jugadorMasCercano.posicion
            x, y = self.posicion
            
            margen = 50 #Margen de control del jugador

            if not self.estaAtacando():
                if x_jugador - x < -margen:
                    Personaje.mover(self,IZQUIERDA)
                elif x_jugador - x > margen:
                    Personaje.mover(self,DERECHA)
                elif abs(x_jugador - x) <= margen and abs(y_jugador - y) <= margen:
                    self.atacar()
                    Personaje.mover(self,QUIETO)

                if y_jugador - y < -margen and abs(x_jugador-x)<=margen:
                    Personaje.mover(self,ARRIBA) #Control del salto del enemigo si pasas por encima en un rango.

        # Si este personaje no está en pantalla, no hará nada
        else:
            Personaje.mover(self,QUIETO)


    def morir(self):
        GestorRecursos.CargarEfectoSonido("carcelero0.wav", 0.2, True, 5)
        Personaje.mover(self,QUIETO)
        self.numImagenPostura = 0
        self.muriendo = True


    def estaAtacando(self):
        if self.numPostura == SPRITE_ATACANDO:
            # Comprueba si el sprite está atacando y justo si está en el momento
            # de la estocada
            if self.numImagenPostura == 2:
                return True
        return False

    def atacar(self):
        if self.numPostura != SPRITE_ATACANDO:
            self.numPostura = SPRITE_ATACANDO
            self.numImagenPostura = 0
        GestorRecursos.CargarEfectoSonido("Espada1.wav", 0.1, True, 6)
