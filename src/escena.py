# -*- encoding: utf-8 -*-

import pygame

ANCHO_PANTALLA = 800
ALTO_PANTALLA = 600

# -------------------------------------------------
# Clase Escena con lo metodos abstractos

class Escena:
    ''' Clase abstracta de Escena '''

    def __init__(self, director):
        self.director = director

    def update(self, *args):
        raise NotImplemented("Tiene que implementar el metodo update.")

    def eventos(self, *args):
        raise NotImplemented("Tiene que implementar el metodo eventos.")

    def dibujar(self):
        raise NotImplemented("Tiene que implementar el metodo dibujar.")

class EscenaPygame(Escena):
    ''' Clase abstracta de EscenaPygame. Tipo de escena que usa la libreria
        pygame '''

    def __init__(self, director):
        ''' Inicializa la libreria pygame y la ventana del juego '''

        Escena.__init__(self, director)
        pygame.init()
        self.screen = pygame.display.set_mode((ANCHO_PANTALLA, ALTO_PANTALLA))
        pygame.display.set_caption('Lezo: Una victoria, una venganza')