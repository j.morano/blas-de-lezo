# -*- encoding: utf-8 -*-

import pygame
from pygame.locals import *
from escena import *
from gestorRecursos import *
import menu
# import fase1

from GUI import Boton


################################################################################


class TextoMuerte(Boton):
    ''' Texto de muerte '''

    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, "¡HAS MUERTO!", GestorRecursos.font, 60, GestorRecursos.RED, (150, 130))


    def actualizarColor(self, pantalla, color_nuevo):
        return


    def accion(self):
        return



class TextoMuerteSombra(Boton):
    ''' Sombra del texto de muerte '''

    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, "¡HAS MUERTO!", GestorRecursos.font, 60, GestorRecursos.BLACK, (152, 132))


    def actualizarColor(self, pantalla, color_nuevo):
        return


    def accion(self):
        return



class TextoJugar(Boton):
    ''' Boton para continuar el juego '''

    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, "CONTINUAR", GestorRecursos.font, 40, GestorRecursos.WHITE, (270, 430))


    def accion(self):
        self.pantalla.menu.ejecutarJuego()



class TextoSalir(Boton):
    ''' Boton para salir del juego '''

    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, "SALIR", GestorRecursos.font, 40, GestorRecursos.WHITE, (345, 550))


    def accion(self):
        self.pantalla.menu.salirPrograma()



class TextoMenu(Boton):
    ''' Boton para ir al menu '''

    def __init__(self, pantalla):
        Boton.__init__(self, pantalla, "MENU", GestorRecursos.font, 40, GestorRecursos.WHITE, (335, 490))


    def accion(self):
        self.pantalla.menu.irMenu()



class PantallaMuerte:
    ''' Pantalla de muerte general '''

    def __init__(self, menu, nombreImagen):

        self.menu = menu

        # Se carga la imagen de fondo
        (self.imagen, self.imagen_rect) = GestorRecursos.CargarImagen(nombreImagen, True)

        # Se tiene una lista de elementos GUI y de animaciones
        self.elementosGUI = []
        self.animaciones = []
        self.elementoClic = None


    def eventos(self, lista_eventos):
        ''' Metodo para tratar con los eventos de esta escena '''

        for evento in lista_eventos:

            # Eventos de situacion del raton
            if evento.type == MOUSEMOTION:
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        elemento.actualizarColor(self.menu.screen, GestorRecursos.RED)
                    else:
                        elemento.actualizarColor(self.menu.screen, GestorRecursos.WHITE)

            # Eventos de clickado pero no de soltado (no confirmada la seleccion)
            if evento.type == MOUSEBUTTONDOWN:
                self.elementoClic = None
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        self.elementoClic = elemento

            # Eventos de soltado del click (confirmada la selección)
            if evento.type == MOUSEBUTTONUP:
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        if (elemento == self.elementoClic):
                            elemento.accion()


    def dibujar(self, pantalla):
        ''' Metodo para dibujar el menú de gameover'''

        # Dibujamos imagen de fondo
        pantalla.blit(self.imagen, self.imagen.get_rect())

        # Dibujamos las animaciones
        for animacion in self.animaciones:
            animacion.dibujar(pantalla)

        # Dibujamos los botones
        for elemento in self.elementosGUI:
            elemento.dibujar(pantalla)



class PantallaMuerteInicial(PantallaMuerte):
    ''' Pantalla de muerte concreta '''

    def __init__(self, menu):
        PantallaMuerte.__init__(self, menu, 'gameover.jpg')
        textoMuerteSombra = TextoMuerteSombra(self)
        textoMuerte = TextoMuerte(self)
        textoJugar = TextoJugar(self)
        textoSalir = TextoSalir(self)
        textoMenu = TextoMenu(self)
        self.elementosGUI.append(textoMuerteSombra)
        self.elementosGUI.append(textoMuerte)
        self.elementosGUI.append(textoJugar)
        self.elementosGUI.append(textoSalir)
        self.elementosGUI.append(textoMenu)


# -------------------------------------------------
# Clase MenuPygame, la escena en sí, en Pygame

class GameOverMenu(EscenaPygame):
    ''' Escena de menu de muerte del juego '''

    def __init__(self, director):

        EscenaPygame.__init__(self, director)

        # Si es la primera vez que se dibuja se hacen los efectos que se indican
        self.primero = True

        # Creamos las pantallas y las introducimos en la lista. Inicializamos un
        # indicador de la pantalla actual y mostramos esa pantalla
        self.listaPantallas = []
        self.listaPantallas.append(PantallaMuerteInicial(self))
        self.pantallaActual = 0
        self.mostrarPantallaInicial()


    def update(self, *args):
        ''' Como cada pantalla es estática, no se actualiza nada '''
        return


    def eventos(self, lista_eventos):
        ''' Metodo para tratar los eventos de esta escena '''

        # Eventos para salir del programa
        for evento in lista_eventos:
            if evento.type == KEYDOWN:
                if evento.key == K_ESCAPE:
                    self.salirPrograma()
            elif evento.type == pygame.QUIT:
                self.director.salirPrograma()

        # Se delega el resto de eventos a las pantallas concretas
        self.listaPantallas[self.pantallaActual].eventos(lista_eventos)


    def dibujar(self, pantalla):
        ''' Metodo para dibujar las pantallas '''

        # Si es la primera vez que se dibuja se hacen los efectos que se indican
        if self.primero:
            GestorRecursos.pararMusica()
            self.primero = False
            GestorRecursos.CargarMusica("GameOver.wav", 0.25, 0)

        # El resto del dibujado se delega a la pantalla actual
        self.listaPantallas[self.pantallaActual].dibujar(pantalla)


    def salirPrograma(self):
        ''' Metodo para salir del juego '''

        self.director.salirPrograma()


    def ejecutarJuego(self):
        ''' Metodo para continuar en la pantalla en la que se había muerto '''

        # self.director.printPila()

        # Le decimos al director que desapile la escena del menu de muerte
        self.director.salirEscena()
        if (not self.director.pilaVacia()):

            # Reinicializamos la escena que esté en la cima para no seguir como
            # lo habíamos dejado cuando habíamos muerto y cambiamos la escena
            # que hay en la pila por la escena reinicializada
            escenaReiniciada = self.director.cimaPila().re_init()
            self.director.cambiarEscena(escenaReiniciada)

        # Sino es la fase 1, donde se cargaba la música, entonces se vuelve a
        # cargar
        if (not escenaReiniciada.getFase() == 1):
            GestorRecursos.BajarMusica(200)
            GestorRecursos.CargarMusica('PirateMap2.mp3', 0.07, -1)


    def irMenu(self):
        ''' Metodo para salir al menu del juego '''

        # Vaciamos la pila hasta que solo quede la escena del menú
        while (not self.director.pilaUnaEscena()):
            self.director.salirEscena()

        # Creamos la escena menú y cambiamos el menú que había por el nuevo
        escena = menu.Menu(self.director)
        self.director.cambiarEscena(escena)


    def mostrarPantallaInicial(self):
        ''' Metodo para situar la pantalla inicial en la primera '''

        self.pantallaActual = 0