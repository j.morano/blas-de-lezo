# -*- coding: utf-8 -*-

import pygame
from escena import *
from personajes import *
from pygame.locals import *
from personajes import DANO, IZQUIERDA, DERECHA

from gestorRecursos import GestorRecursos

from pausa import Pausa
import time

import json

################################################################################


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# Constantes
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

# Los bordes de la pantalla para hacer scroll horizontal
MINIMO_X_JUGADOR = 200
MAXIMO_X_JUGADOR = ANCHO_PANTALLA - MINIMO_X_JUGADOR

# ------------------------------------------------------------------------------
# Clase Fase

class Fase(EscenaPygame):
    def __init__(self, director, fase):

        self.fase = fase

        # Primero invocamos al constructor de la clase padre
        EscenaPygame.__init__(self, director)

        # Variables que describen cierto estado de la fase
        self.pausa = False
        self.llaveRecogida = False
        self.desbloqueado = False
        self.puntuacion = None


        # Variables relacionadas con el tiempo límite de la fase
        self.tiempoInicio = None
        self.tiempoFin = None
        self.tiempoRestante = None


        self.conEscudo = None

        
        # ==================  Creación de grupos  ====================
        self.grupoPlataformasEscaleras = pygame.sprite.Group()
        self.grupoEnemigos = pygame.sprite.Group()
        self.grupoJefes = pygame.sprite.Group()
        self.grupoPlataformas = pygame.sprite.Group()
        self.grupoPlataformasColision = pygame.sprite.Group()
        self.grupoPlataformasPinchos = pygame.sprite.Group()
        self.grupoJugadores = pygame.sprite.Group()
        self.grupoPlataformasFuego = pygame.sprite.Group()


        #######################################################################
        #######################################################################

        # Qué parte del decorado estamos visualizando
        self.scrollx = 0

        if fase == 1:
            self.transicion = True
        else:
            self.transicion = False

        # Se cargan las características particulares de la fase a partir de su
        # archivo .json
        json_file='FASES/fase'+str(fase)+'.json'
        json_data=open(json_file)
        data = json.load(json_data)
        json_data.close()


        # ==================  Decorado  ====================

        # Creamos el decorado y el fondo
        self.decorado = Decorado(data["decorado"][0], data["decorado"][1])


        # ==================  Condiciones de la fase  ====================

        self.llaveNecesaria = data["llave"]
        

        # ==================  Plataformas del decorado  ====================
        
        # Las plataformas sobre las que pueden estar y saltar los personajes
        self.anadirPlataformas(data["coordenadas"]["suelos"], \
            [self.grupoPlataformas])

        try:
            # Las plataformas sobre las que pueden estar y saltar los personajes
            self.anadirPlataformas(data["coordenadas"]["escaleras"], \
                [self.grupoPlataformasEscaleras, self.grupoPlataformas])
        except KeyError:
            pass

        # Pinchos
        self.anadirPlataformas(data["coordenadas"]["pinchos"], \
            [self.grupoPlataformasPinchos, self.grupoPlataformas])


        try:
            # Fuego
            self.anadirPlataformas(data["coordenadas"]["fuego"], \
                [self.grupoPlataformasFuego, self.grupoPlataformas])
        except KeyError:
            pass


        try:
            # Tiempo límite
            self.tiempoLimite = data["tiempoLimite"]
        except KeyError:
            # Valor por defecto
            self.tiempoLimite = 70


        # Plataformas epseciales, con un comportamiento particular cuando se
        # colisiona con ellas
        self.anadirPlataformasEspeciales(data["coordenadas"]["especiales"], \
            [self.grupoPlataformasColision])


        # ==================  Enemigos  ====================

        # Añadimos enemigos y creamos su grupo
        self.anadirPersonajes(data["coordenadas"]["enemigos"],'carcelero',[self.grupoEnemigos])

        try:
            # Jefe Final
            self.anadirPersonajes(data["coordenadas"]["jefeFinal"], 'jefe',\
                [self.grupoEnemigos,self.grupoJefes])
            self.jefeFinal = True
        except KeyError:
            pass
        
        # ==================  Jugadores  ====================

        # Creamos los sprites de los jugadores
        self.anadirPersonajes(data["coordenadas"]["jugador"], 'jugador', [self.grupoJugadores])
        self.jugador1 = self.grupoJugadores.sprites()[0]
        
        # ==================  Adición a grupos generales  ====================

        # Grupo con los Sprites que se mueven
        self.grupoSpritesDinamicos = pygame.sprite.Group(self.grupoJugadores, self.grupoEnemigos)
        
        # Creamos otro grupo con todos los Sprites
        self.grupoSprites = pygame.sprite.Group(self.grupoJugadores, self.grupoEnemigos , self.grupoPlataformas, self.grupoPlataformasPinchos, self.grupoPlataformasEscaleras, self.grupoPlataformasColision)


        #######################################################################
        #######################################################################
        


    # Devuelve True o False según se haya tenido que desplazar el scroll o no
    def actualizarScrollOrdenados(self, jugador):
        
        # Si el jugador se está moviendo y el jugador se aproxima al límite izquierdo
        # de la pantalla
        if not jugador.estaQuieto() and jugador.rect.left<MINIMO_X_JUGADOR:
            # Si se está en el límite izquierdo de la fase, mantener ahí
            if self.scrollx <= 0:
                self.scrollx = 0
                if jugador.rect.left<=0:
                    jugador.establecerPosicion((0, jugador.posicion[1]))
                return False
            else: # Si no, calcular desplazamiento
                desplazamiento = MINIMO_X_JUGADOR - jugador.rect.left
                self.scrollx = self.scrollx - desplazamiento;
                return True # Se ha actualizado el scroll

        # Si el jugador se aproxima al límite izquierdo de la pantalla con
        # un cierto margen para evitar problemas (tirones) con la variación
        # de tamaño de los rects. de los sprites
        if (jugador.rect.left+150)>MAXIMO_X_JUGADOR:

            # Si el escenario ya está a la derecha del todo, no lo movemos más
            if self.scrollx + ANCHO_PANTALLA >= self.decorado.rect.right:
                self.scrollx = self.decorado.rect.right - ANCHO_PANTALLA
                if jugador.rect.right>=ANCHO_PANTALLA:
                    # En su lugar, colocamos al jugador a la derecha de todo
                    jugador.establecerPosicion((self.decorado.rect.right-jugador.rect.width, jugador.posicion[1]))
                return False

            # Si sí se puede hacer scroll a la derecha
            else:
                # Se calcula cuántos pixeles está fuera del borde, manteniendo el mismo
                # margen antes comentado
                desplazamiento = (jugador.rect.left+150) - MAXIMO_X_JUGADOR

                # ... y calculamos el nivel de scroll actual: el anterior + desplazamiento
                #  (desplazamos a la derecha)
                self.scrollx = self.scrollx + desplazamiento;

                return True # Se ha actualizado el scroll

        return False



    def actualizarScroll(self, jugador1):
        # Se ordenan los jugadores según el eje x, y se mira si hay que actualizar el scroll
        cambioScroll = self.actualizarScrollOrdenados(jugador1)

        # Si se cambió el scroll, se desplazan todos los Sprites y el decorado
        if cambioScroll:
            # Actualizamos la posición en pantalla de todos los Sprites según el scroll actual
            for sprite in iter(self.grupoSprites):
                sprite.establecerPosicionPantalla((self.scrollx, 0))

            # Además, actualizamos el decorado para que se muestre una parte distinta
            self.decorado.update(self.scrollx)



    def update(self, tiempo):

        # Se recupera la puntuación que se tenía
        if self.puntuacion is None:
            self.puntuacion = GestorRecursos.recuperarPuntuacion()

        # Se establecen los valores de tiempo de referencia para los cálculos de tiempos
        # con respecto al temporizador de la fase
        if self.tiempoInicio is None:
            self.tiempoInicio = time.time()
            self.tiempoFin = self.tiempoInicio+self.tiempoLimite

        # Se calcula el nuevo tiempo restante
        self.tiempoRestante = int(self.tiempoFin-time.time())

        if self.tiempoRestante <= 0:
            # Si no queda tiempo el jugador se muere
            self.jugador1.setHealth(-1, self.director)

        # Comprueba si no quedan jefes que eliminar para poder avanzar fase
        if not self.desbloqueado:
            if not self.grupoJefes:
                self.desbloqueo()

        # Primero, se indican las acciones que van a hacer los enemigos segun como esten los jugadores
        for enemigo in iter(self.grupoEnemigos):
            enemigo.mover_cpu(self.jugador1)

        # Esta operación es aplicable también a cualquier Sprite que tenga algún tipo de IA
        # En el caso de los jugadores, esto ya se ha realizado

        # Actualizamos los Sprites dinámicos teniendo en cuenta las plataformas de la fase
        # y de tal forma que se simula que cambian todos a la vez
        # Esta operación de update ya comprueba que los movimientos sean correctos
        #  y, si lo son, realiza el movimiento de los Sprites
        self.grupoSpritesDinamicos.update(self.grupoPlataformas, self.grupoPlataformasColision, self.grupoPlataformasPinchos, self.grupoPlataformasFuego, self.grupoPlataformasEscaleras, self.director, tiempo)
        

        # Comprobamos si hay colisión entre algún jugador y algún enemigo y se estudia el contexto
        # de la colisión y el estado de los sprites que colisionan
        colisiones = pygame.sprite.groupcollide(self.grupoEnemigos, self.grupoJugadores, False, False)
        if colisiones!={}:
            
            for enemigo in colisiones:
                if self.jugador1.puedeAtacar() \
                and not enemigo.estaMuriendo() \
                and self.jugador1.estaAtacando() \
                and ((self.jugador1.rect.x<=enemigo.rect.x and self.jugador1.miraHacia()==DERECHA) \
                     or (self.jugador1.rect.x>enemigo.rect.x and self.jugador1.miraHacia()==IZQUIERDA)):
                        # print(enemigo)
                        self.jugador1.gastarAtaque()
                        enemigo.morir()
                        self.puntuacion += 15
                        if isinstance(enemigo, JefeFinal):
                            # enemigo con inmunidad temporal
                            self.conEscudo = enemigo
                else:
                    if not enemigo.estaMuriendo():
                        if isinstance(enemigo, CarceleroPistolero) or isinstance(enemigo, CarceleroEspadachin):
                            if enemigo.estaAtacando():
                                if isinstance(enemigo, CarceleroPistolero):
                                    self.jugador1.setHealth(self.jugador1.getHealth()-(DANO*3)*tiempo, self.director)
                                else:
                                    self.jugador1.setHealth(self.jugador1.getHealth()-(DANO*4)*tiempo, self.director)
                                GestorRecursos.CargarEfectoSonido("danoPinchos.wav", 0.25, True, 1)
                        else:
                            if isinstance(enemigo, JefeFinal):
                                self.jugador1.setHealth(self.jugador1.getHealth()-(DANO*2)*tiempo, self.director)
                            else:
                                self.jugador1.setHealth(self.jugador1.getHealth()-(DANO*0.7)*tiempo, self.director)
                            GestorRecursos.CargarEfectoSonido("danoPinchos.wav", 0.15, True, 1)


        # Se comprueban las colisiones de los sprites dinámicos (personajes en nuestro caso) con las plataformas
        # con comportamientos particulares en caso de colisiones
        colisiones = pygame.sprite.groupcollide(self.grupoSpritesDinamicos, self.grupoPlataformasColision, False, False)
        if colisiones!={}:
            for sprite, colision in colisiones.items():
                colision = colision[0]
                if colision.nombre == "paredDerecha": # Colisiones hacia la derecha "muros"
                    sprite.establecerPosicion((colision.rect.left - sprite.rect.width + sprite.scroll[0], sprite.posicion[1]))
                if colision.nombre == "paredIzquierda": # Colisiones hacia la izquierda "muros"
                    sprite.establecerPosicion((colision.rect.left + colision.rect.width + sprite.scroll[0], sprite.posicion[1]))
                if colision.nombre == "techo": # Colisiones hacia arriba, "techos" -> caer hacia abajo
                    sprite.setVelocidad(vy=-sprite.getVelocidad()[1]*0.25)
                    sprite.establecerPosicion((sprite.posicion[0], colision.rect.bottom + sprite.rect.height))
                if isinstance(sprite, Jugador):
                    # Colisiones con comportamientos particulares para el jugador
                    if colision.nombre == "cargarFase": # Puertas para avanzar fase
                        if (not self.llaveNecesaria or (self.llaveNecesaria and self.llaveRecogida)) and self.desbloqueado:
                            GestorRecursos.guardarEstado(self.puntuacion, self.fase)
                            GestorRecursos.CargarEfectoSonido("PuertaLlave.wav", 0.25, True, 4)
                            self.director.salirEscena() # desapila para ir a la siguiente fase del juego
                    if colision.nombre == "jefeFinal": # Colisión para entrar en el contexto de la 
                                                       # batalla con el jefe final (alcaide)
                        colision.kill()
                        GestorRecursos.pararMusica()
                        GestorRecursos.CargarMusica("JefeFinal.mp3", 0.20,-1)
                        self.bloqueo()
                    if colision.nombre == "llave": # Llaves que aparecen en las fases
                        GestorRecursos.CargarEfectoSonido("llaves.wav", 0.20, True, 4)
                        self.llaveRecogida = True
                        colision.kill()
                    if colision.nombre == "itemsRecoger": # Items a recoger que dan bonificaciones
                        GestorRecursos.CargarEfectoSonido("item.wav", 0.25, True, 4)
                        self.puntuacion += 60
                        colision.kill()
                    if colision.nombre == "desnaux": # Carlos Desnaux, que te ayuda a recuerar la "vida"
                        for spriteColision in self.grupoPlataformasColision:
                            if spriteColision.nombre == "mensaje1":
                                GestorRecursos.CargarEfectoSonido("page.wav", 0.20, True, 4)
                                spriteColision.kill()
                        sprite.setHealth(100, self.director)


        # Actualizamos el scroll
        self.actualizarScroll(self.jugador1)



    def pintarHUD(self, pantalla):

        # Pintar salud del jugador
        if self.jugador1.getHealth() > 75:
            jugador_barra_color = GestorRecursos.GREEN
        elif self.jugador1.getHealth() > 50:
            jugador_barra_color = GestorRecursos.YELLOW
        else:
            jugador_barra_color = GestorRecursos.RED

        pygame.draw.rect(pantalla, jugador_barra_color, (30,30,self.jugador1.getHealth(),10))
        (corazon,corazon_rect) = GestorRecursos.CargarImagen("corazon.png",False,-1)
        pantalla.blit(corazon, (150, 20, corazon_rect.width,corazon_rect.height))

        # Pintar icono de la llave si esta es necesaria
        if self.llaveNecesaria:
            pygame.draw.rect(pantalla, jugador_barra_color, (30,30,self.jugador1.getHealth(),10))
            (llave,llave_rect) = GestorRecursos.CargarImagen("llave.png",True,-1, escalado=(65, 50))
            if not self.llaveRecogida:
                llave.set_alpha(70)
            pantalla.blit(llave, (180, 13, llave_rect.width,llave_rect.height))

        # Pintar puntuación actual
        dibujoPuntuacion = GestorRecursos.text_format(str(self.puntuacion), GestorRecursos.font, 25, GestorRecursos.ORANGE)
        pantalla.blit(dibujoPuntuacion,(700,20))

        # Pintar tiempo restante para completar la fase
        (reloj,reloj_rect) = GestorRecursos.CargarImagen("reloj_arena.png",True,-1, escalado=(30, 30))
        pantalla.blit(reloj, (410, 19, reloj_rect.width,reloj_rect.height))
        
        dibujoTiempo = GestorRecursos.text_format(str(self.tiempoRestante), GestorRecursos.font, 25, GestorRecursos.ORANGE)
        pantalla.blit(dibujoTiempo,(445,20))


        # Pintar estado de recarga del ataque
        recargaAtaque = self.jugador1.getPorcIntervalo()
        if recargaAtaque > 75:
            jugador_barra_color = GestorRecursos.LIGHTBLUE
        elif recargaAtaque > 50:
            jugador_barra_color = GestorRecursos.BLUE
        else:
            jugador_barra_color = GestorRecursos.DARKBLUE

        pygame.draw.rect(pantalla, jugador_barra_color, (30,60, recargaAtaque, 10))



    def pintarInfoDinamica(self, pantalla):
        if self.conEscudo is not None:
            if self.conEscudo.alive():
                # Pinta el escudo del jefe si este está en un momento de inmunidad
                if self.conEscudo.estaEscudado():
                    (reloj,reloj_rect) = GestorRecursos.CargarImagen("escudo.png",True,-1, escalado=(20, 20))
                    pantalla.blit(reloj, (self.conEscudo.rect.left-5, self.conEscudo.rect.top-5, reloj_rect.width,reloj_rect.height))
                else:
                    self.conEscudo = None


  
    def dibujar(self, pantalla):

        # Si venimos de la cinemática, hacer transición
        if self.transicion:
            self.transicion = False
            GestorRecursos.BajarMusica(1500)
            pantalla.fill(GestorRecursos.BLACK)
            pygame.display.update()
            GestorRecursos.CargarMusica('PirateMap2.mp3', 0.07, -1)


        # Se dibuja el decorado en la pantalla en primer lugar
        self.decorado.dibujar(pantalla)

        # Se pinta la información dinámica interesante que puede haber en cada momento
        self.pintarInfoDinamica(pantalla)

        # Se dibujan los Sprites
        self.grupoSprites.draw(pantalla)

       # Se pinta el HUD con información relevante para el jugador
        self.pintarHUD(pantalla)



    def eventos(self, lista_eventos):
        # Miramos a ver si hay algun evento de salir del programa
        for evento in lista_eventos:
            # Si se quiere salir, se le indica al director
            if evento.type == pygame.QUIT:
                self.director.salirPrograma()

        # Indicamos la acción a realizar segun la tecla pulsada para cada jugador
        teclasPulsadas = pygame.key.get_pressed()
        self.jugador1.mover(teclasPulsadas, K_UP, K_DOWN, K_LEFT, K_RIGHT)
        self.jugador1.checkAttack(teclasPulsadas, K_SPACE)
        # Si se pulsa la P, detener la fase y acceder al menú de pausa
        if teclasPulsadas[K_p]:
            if not self.pausa:
                self.pausa = True
                escena = Pausa(self.director)
                self.director.apilarEscena(escena)
        

    # Vuelve a cargar la fase (reinicia la fase)
    def re_init(self):
        return Fase(self.director, self.fase)

    def getFase(self):
        return self.fase

    # Función que se ejecuta al volver del menú de pausa
    def volver(self):
        self.pausa = False
        self.tiempoFin = int(time.time()+self.tiempoRestante)


    def bloqueo(self):
        self.desbloqueado = False

    def desbloqueo(self):
        self.desbloqueado = True



    # =============  Funciones para añadir elementos a la fase  =============

    def anadirPlataformas(self, lista_plataformas, grupos, textura=None):
        for plataforma in lista_plataformas:
            try:
                x,y,ancho,alto,nombre = plataforma
            except ValueError:
                x,y,ancho,alto = plataforma
                nombre = None
            nueva_plataforma = Plataforma(pygame.Rect(x, y, ancho, alto), nombre)
            self.anadirAGrupos(nueva_plataforma, grupos)


    def anadirPlataformasEspeciales(self, listas, grupos, textura=None):
        for tipo, lista in listas.items():
            for plataforma in lista:

                nombre = str(tipo)
                if nombre == 'itemsRecoger':
                    x,y = plataforma
                    ancho, alto = 22, 22
                else:
                    x,y,ancho,alto = plataforma
                
                nueva_plataforma = Plataforma(pygame.Rect(x, y, ancho, alto), nombre, textura)
                self.anadirAGrupos(nueva_plataforma, grupos)


    def anadirPersonajes(self, lista_coordenadas, tipo, grupos):
        if tipo != 'carcelero':
            for coordenadas in lista_coordenadas:
                x,y = coordenadas
                personaje = FactoriaPersonaje.obtenerPersonaje(tipo)
                personaje.establecerPosicion((x, y))
                self.anadirAGrupos(personaje, grupos)
        else:
            for coordenadas in lista_coordenadas:
                x,y = coordenadas
                # Selección aleatoria según probabilidades del tipo de enemigo
                # a añadir
                selector = random.random()
                if selector < 0.3:
                    tipo = 'carceleroSimple'
                elif selector < 0.5:
                    tipo = 'carceleroEspadachin'
                elif selector < 0.8:
                    tipo = 'carceleroPistolero'
                elif selector <= 1:
                    tipo = 'carcelero'
                personaje = FactoriaPersonaje.obtenerPersonaje(tipo)
                personaje.establecerPosicion((x, y))
                self.anadirAGrupos(personaje, grupos)
                tipo = 'carcelero'



    def anadirAGrupos(self, elemento, grupos):
        for grupo in grupos:
            grupo.add(elemento)





# ------------------------------------------------------------------------------
# Clase FactoriaPersonaje
# 
#   - Encargada de devolver un objeto del tipo indicado

class FactoriaPersonaje():

    @staticmethod
    def obtenerPersonaje(tipo):
        if tipo == 'jugador':
            return Jugador()
        elif tipo == 'carcelero':
            return Carcelero()
        elif tipo == 'carceleroSimple':
            return CarceleroSimple()
        elif tipo == 'carceleroEspadachin':
            return CarceleroEspadachin()
        elif tipo == 'carceleroPistolero':
            return CarceleroPistolero()
        elif tipo == 'jefe':
            return JefeFinal()
        else:
            print('Error: Tipo de personaje desconocido')
  


# ------------------------------------------------------------------------------
# Clase Plataforma
# 
#   - Para la creación de plataformas


class Plataforma(MiSprite):
    def __init__(self, rectangulo, nombre=None, textura=None):
        # Primero invocamos al constructor de la clase padre
        MiSprite.__init__(self)
        # Rectángulo con las coordenadas en pantalla que ocupará
        self.rect = rectangulo
        # Nombre de la plataforma utilizado para comprobaciones particulares
        self.nombre = nombre
        # Y lo situamos de forma global en esas coordenadas
        self.establecerPosicion((self.rect.left, self.rect.bottom))
        if textura is None:
            if (nombre == 'muro'):
                (self.image, _) = GestorRecursos.CargarImagen("Muro.png", False)
            elif (nombre == 'llave'):
                (self.image, _) = GestorRecursos.CargarImagen("llave.png",True,-1, escalado=(50, 40))
            elif (nombre == 'itemsRecoger'):
                selector = random.random()
                if selector < 1/7:
                    (self.image, _) = GestorRecursos.CargarImagen("moneda.png",True,-1, escalado=(22, 22))
                elif selector < 2/7:
                    (self.image, _) = GestorRecursos.CargarImagen("mapa.png",True,-1, escalado=(22, 22))
                elif selector < 3/7:
                    (self.image, _) = GestorRecursos.CargarImagen("mapa_2.png",True,-1, escalado=(22, 22))
                elif selector < 4/7:
                    (self.image, _) = GestorRecursos.CargarImagen("brujula.png",True,-1, escalado=(22, 22))
                elif selector < 5/7:
                    (self.image, _) = GestorRecursos.CargarImagen("botella.png",True,-1, escalado=(22, 22))
                elif selector < 6/7:
                    (self.image, _) = GestorRecursos.CargarImagen("barril.png",True,-1, escalado=(22, 22))
                elif selector <= 7/7:
                    (self.image, _) = GestorRecursos.CargarImagen("catalejo.png",True,-1, escalado=(22, 22))
            elif (nombre == 'desnaux'):
                (self.image, _) = GestorRecursos.CargarImagen("desnaux.png",True,-1, escalado=(39, 48))
            elif (nombre == 'mensaje1'):
                (self.image, _) = GestorRecursos.CargarImagen("mensaje1.png",True,-1, escalado=(250, 180))
            else:
                self.image = pygame.Surface((0, 0))
        else:
            if (textura == 'muro'):
                (self.image, _) = GestorRecursos.CargarImagen("Muro.png", False)


# ------------------------------------------------------------------------------
# Clase Decorado
# 
#   - Para la carga del decorado

class Decorado:
    def __init__(self, imagen_decorado, distancia):
        
        (self.imagen, self.rect) = GestorRecursos.CargarImagen(imagen_decorado, True, escalado = (distancia, 600))

        self.rect.bottom = ALTO_PANTALLA

        # La subimagen que estamos viendo
        self.rectSubimagen = pygame.Rect(0, 0, ANCHO_PANTALLA, ALTO_PANTALLA)
        self.rectSubimagen.left = 0 # El scroll horizontal empieza en la posicion 0 por defecto

    def update(self, scrollx):
        self.rectSubimagen.left = scrollx

    def dibujar(self, pantalla):
        pantalla.blit(self.imagen, self.rect, self.rectSubimagen)
