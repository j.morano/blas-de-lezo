# -*- encoding: utf-8 -*-

import pygame
import sys
from escena import *
from pygame.locals import *

FPS = 60

class Director():
    ''' Clase director que mantiene las escenas en una pila y ejecuta el bucle
    	principal de ejecucion. Ademas realiza el bucle de eventos de pygame '''

    instanceDirector = None
    hasInstance = False

    @classmethod
    def getInstanceDirector(self):
        ''' Implementacion simple del patron singleton '''

        if (not self.hasInstance):  	
            self.instanceDirector = Director()
            self.hasInstance = True
            return self.instanceDirector


    def __init__(self):

    	# Ocupacion de la pila
        self.ocupado = False

        # Pila de escenas
        self.pila = []

        # Para terminar el bucle de eventos pygame en una escena
        self.salir_escena_pygame = False


    def buclePygame(self, escena):
        ''' Bucle de eventos de escenas pygame '''

        # Reloj de pygame
        reloj = pygame.time.Clock()

        # Ponemos el flag de salir de la escena a False
        self.salir_escena_pygame = False

        # Eliminamos todos los eventos producidos antes de entrar en el bucle
        pygame.event.clear()

        # Bucle de la escena
        while not self.salir_escena_pygame:

            # Sincronizar el juego a 60 fps
            tiempo_pasado = reloj.tick(FPS)

            # Pasamos los eventos a la escena
            escena.eventos(pygame.event.get())

            # Actualiza la escena
            escena.update(tiempo_pasado)

            # Se dibuja en pantalla
            escena.dibujar(escena.screen)

            #Doble buffering
            pygame.display.flip()


    def ejecutar(self):
        ''' Bucle principal del juego. Mientras haya escenas se ejecuta el juego
        '''

        # Inicializamos la libreria de pygame (si no esta inicializada ya)
        pygame.init()

        # Creamos la pantalla de pygame (si no esta creada ya)
        self.screen = pygame.display.set_mode((ANCHO_PANTALLA, ALTO_PANTALLA))
        pygame.display.set_caption('Lezo: Una victoria, una venganza')

        # Bucle del juego
        while (len(self.pila)>0):

            # Se coge la escena a ejecutar como la que este en la cima de la pila
            escena = self.pila[len(self.pila)-1]

            # Si la escena es de pygame
            if isinstance(escena, EscenaPygame):

                # Ejecutamos el bucle
                self.buclePygame(escena)

            # Si es otro tipo de escena
            # elif isinstance(escena, OtroTipoEscena
            # ...

            else:
                raise Exception('No se que tipo de escena es')

        # Finalizamos la libreria de pygame y cerramos las ventanas
        pygame.quit()


    def pararEscena(self):
        ''' Hace que se termine la escena actual sin modificar el estado de la
            pila de escenas '''

        if (len(self.pila)>0):
            escena = self.pila[len(self.pila)-1]
            # Si la escena es de pygame
            if isinstance(escena, EscenaPygame):
                # Indicamos en el flag que se quiere salir de la escena
                self.salir_escena_pygame = True
            # Si es una escena de pyglet
            elif isinstance(escena, EscenaPyglet):
                # Salimos del bucle de pyglet
                pyglet.app.exit()
            else:
                raise Exception('No se que tipo de escena es')


    def salirEscena(self):
        ''' Termina la escena actual y la elimina de la pila '''

        self.ocupado = True
        self.pararEscena()
        self.pila.pop()
        self.ocupado = False


    def directorOcupado(self):
        ''' Devuelve la ocupación del director '''

        return self.ocupado
        

    def pilaVacia(self):
        ''' Devuelve True si la pila esta vacia y False en caso contrario '''

        return (len(self.pila)==0)


    def pilaUnaEscena(self):
        ''' Devuelve True si la pila tiene solo un elemento (que será la escena
            menu) '''

        return (len(self.pila)==1)


    def cimaPila(self): #Comprobar si vacía
        ''' Devuelve la escena de la cima de la pila '''

        return self.pila[len(self.pila)-1]


    def salirPrograma(self):
        ''' Vacia la pila de escenas, lo que genera que se salga del bucle de
            ejecución del juego '''

        self.pararEscena()

        # Vaciamos la lista de escenas pendientes
        self.pila = []


    def cambiarEscena(self, escena):
        ''' Termina la escena actual y cambia la escena de la cima por la escena
            del argumento '''

        self.pararEscena()

        # Eliminamos la escena actual de la pila (si la hay)
        if (len(self.pila)>0):
            self.pila.pop()

        # Ponemos la escena pasada en la cima de la pila
        self.pila.append(escena)


    def apilarEscena(self, escena):
        ''' Termina la escena actual y añade en la cima de la pila la escena del
            argumento '''

        self.pararEscena()
        self.pila.append(escena)


    def printPila(self):
        ''' Imprime el contenido de la pila en la consola '''

        for element in self.pila:
            print(element)

