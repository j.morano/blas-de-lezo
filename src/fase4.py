# -*- coding: utf-8 -*-

import pygame
from gestorRecursos import *

from fase import Fase

################################################################################


# -------------------------------------------------
# Clase Fase

class Fase4(Fase):
        
    # Función encargada de crear las plataformas bloqueantes para no poder escapar
    # del recinto de lucha con el jefe (alcaide)
    def bloqueo(self):

        self.desbloqueado = False

        self.grupoPlataformasBloqueo = pygame.sprite.Group()

        plataformasBloqueoIzquierda = {'paredIzquierda':[[1425, 415, 14, 110],[2835, 415, 14, 110]]}

        plataformasBloqueoDerecha = {'paredDerecha':[[1410, 415, 14, 110],[2820, 415, 14, 110]]}


        self.anadirPlataformasEspeciales(plataformasBloqueoIzquierda,[self.grupoPlataformasColision, self.grupoSprites,self.grupoPlataformasBloqueo])

        self.anadirPlataformasEspeciales(plataformasBloqueoDerecha,[self.grupoPlataformasColision, self.grupoSprites,self.grupoPlataformasBloqueo], textura='muro')


    # Función encargada de eliminar las plataformas bloqueantes
    def desbloqueo(self):

        for plataforma in iter(self.grupoPlataformasBloqueo):
            plataforma.kill()

        self.desbloqueado = True

        GestorRecursos.CargarEfectoSonido("muerteJefe.wav", 0.10, True, 8, 4000)
        # Transición a silencio
        GestorRecursos.BajarMusica(4000)
        

    # Función de reinicialización de la Fase 4
    def re_init(self):
        return Fase4(self.director, 4)